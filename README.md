# Zonetalk
ZoneTalk was a Projekt for Location Based Information Services Developed By Alexander Sessler (Web, iOS) and Mustafa Senel (Backend).

![zt_zones.jpg](https://bitbucket.org/repo/Gy8dkB/images/4218259751-zt_zones.jpg)
![zt_filters.jpg](https://bitbucket.org/repo/Gy8dkB/images/2877971886-zt_filters.jpg)
![zt_map.png](https://bitbucket.org/repo/Gy8dkB/images/3617006822-zt_map.png)
![zt_message.jpg](https://bitbucket.org/repo/Gy8dkB/images/56832868-zt_message.jpg)
![zt_nav.jpg](https://bitbucket.org/repo/Gy8dkB/images/884670896-zt_nav.jpg)


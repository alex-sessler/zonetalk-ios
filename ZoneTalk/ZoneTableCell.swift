//
//  ZoneTableCell.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 03/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation
import UIKit

class ZoneTableCell : UITableViewCell {

    @IBOutlet weak var zoneTitleLabel: UILabel!
    @IBOutlet weak var zoneDistanceLabel: UILabel!
    var signalImageView: UIImageView?
    
    func loadItem(title: String, distance: String?) {
        zoneTitleLabel.text = title
        
        if (self.signalImageView != nil && self.signalImageView!.hidden == false) {
            self.signalImageView!.removeFromSuperview()
        }
        
        if (distance == nil) {
            zoneDistanceLabel.text = ""
        }
        
        if let distanceNumber = distance!.toInt() {
           self.zoneDistanceLabel.hidden = false
            
            if (distanceNumber < 1000) {
                zoneDistanceLabel.text = distance! + "m"
            } else {
                zoneDistanceLabel.text = ">1km"
            }
        } else {
            if (distance! == "I") {
                drawSignalStrength(3)
            } else if (distance! == "N") {
                drawSignalStrength(2)
            } else if (distance! == "F") {
                drawSignalStrength(1)
            } else {
                zoneDistanceLabel.text = "?"
            }
            
        }
    }
    
    func drawSignalStrength(strength: Int) {

        let signalImage = UIImage(named: Constants.getImageNameForSignal(strength))
        self.signalImageView = UIImageView(image: signalImage)
        self.signalImageView!.frame = CGRectMake((self.zoneDistanceLabel.frame.maxX - zoneDistanceLabel.frame.height), self.zoneDistanceLabel.frame.minY, zoneDistanceLabel.frame.height, self.zoneDistanceLabel.frame.height)
        self.zoneDistanceLabel.hidden = true
        
        
        self.contentView.addSubview(self.signalImageView!)
       
        
    }
}

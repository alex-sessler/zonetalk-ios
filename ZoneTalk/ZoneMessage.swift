//
//  ZoneMessage.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 29/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class ZoneMessage: NSManagedObject {

    @NSManaged var created_date: NSDate
    @NSManaged var id: String
    @NSManaged var message: String
    @NSManaged var modified_date: NSDate
    @NSManaged var belongs_to: ZoneTalk.Zone

}

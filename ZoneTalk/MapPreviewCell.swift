//
//  MapPreviewCell.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 23/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

protocol MapPreviewCellDelegate {
    func detailButtonTapped(zoneId: String)
    func navigationButtonTapped(zoneId: String)
}

class MapPreviewCell: UITableViewCell {

    var zoneId: String = ""
    var delegate: MapPreviewCellDelegate?
    
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailButton: UIImageView!
    @IBOutlet weak var navigationButton: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(category: String, title: String, zoneId: String) {
        let imageName = Constants.getCategoryImageUrlForName(category, inverse: true)
        
        self.categoryIcon.image = UIImage(named: imageName)
        
        self.titleLabel.text = title
        self.zoneId = zoneId
        
        self.navigationButton.userInteractionEnabled = true
        self.navigationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tappedNavigation:"))
        
        self.detailButton.userInteractionEnabled = true
        self.detailButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "showZoneDetail:"))

    }
    
    func showZoneDetail(recognizer: UITapGestureRecognizer) {
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_PREVIEW_CELL_DETAIL_PRESSED, object: nil, userInfo: ["zoneId":self.zoneId])
    }
    
    func tappedNavigation(recognizer: UITapGestureRecognizer) {
         NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_PREVIEW_CELL_NAVIGATION_PRESSED, object: nil, userInfo: ["zoneId":self.zoneId])
    }
    
    

}

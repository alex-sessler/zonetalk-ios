//
//  iBeaconController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 30/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation

class iBeaconService: NSObject, KTKLocationManagerDelegate {
    
    let locationManager: KTKLocationManager = KTKLocationManager()
    var beaconsInRange : Dictionary<BeaconIdentifier<NSNumber, NSNumber>, CLProximity> = Dictionary<BeaconIdentifier<NSNumber, NSNumber>, CLProximity>()
    
    override init() {
        super.init()
        
        if (KTKLocationManager.canMonitorBeacons())
        {
            var region : KTKRegion = KTKRegion();
            region.uuid = "f7826da6-4fa2-4e98-8024-bc5b71e0893e"; // proximity UUID
            self.locationManager.setRegions([region]);
            
            self.locationManager.delegate = self;

        }
    }
    
    func startMonitoringBeacons() {
        
        self.locationManager.startMonitoringBeacons()
    }
    
    func stopMonitoringBeacons() {
        self.locationManager.stopMonitoringBeacons()
    }
    
    func locationManager(locationManager: KTKLocationManager!, didChangeState state: KTKLocationManagerState, withError error: NSError!) {}
    
    func locationManager(locationManager: KTKLocationManager!, didEnterRegion region: KTKRegion!) {
    }
    
    func locationManager(locationManager: KTKLocationManager!, didExitRegion region: KTKRegion!) {
        
        for beacon in self.beaconsInRange.keys {
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_BEACON_EXITED, object: nil, userInfo: ["major": beacon.major, "minor": beacon.minor])
        }
        
        self.beaconsInRange.removeAll(keepCapacity: false)
    }
    
    func locationManager(locationManager: KTKLocationManager!, didRangeBeacons beacons: [AnyObject]!) {
        
        var activeBeaconsIds = [BeaconIdentifier<NSNumber, NSNumber>]()
        
        let beacons = beacons as [CLBeacon]
        for beacon in beacons {
            
            let beaconId = BeaconIdentifier(major: beacon.major, minor: beacon.minor)
            activeBeaconsIds.append(beaconId)

            if let previousProximity = self.beaconsInRange[beaconId] {
                
                if (beacon.proximity != previousProximity && beacon.proximity != CLProximity.Unknown) {
                    self.beaconsInRange.updateValue(beacon.proximity, forKey: beaconId)
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_BEACON_UPDATED, object: nil, userInfo: ["major": beacon.major, "minor": beacon.minor, "proximity": beacon.proximity.rawValue])
                }
                
            } else {
                self.beaconsInRange.updateValue(beacon.proximity, forKey: beaconId)
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_BEACON_UPDATED, object: nil, userInfo: ["major": beacon.major, "minor": beacon.minor, "proximity": beacon.proximity.rawValue])

            }
          
        }
        
        for beaconId in beaconsInRange.keys {
            if (!contains(activeBeaconsIds, beaconId)) {
                beaconsInRange.removeValueForKey(beaconId)
                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_BEACON_EXITED, object: nil, userInfo: ["major": beaconId.major, "minor": beaconId.minor])
            }
        }
    }
}

struct BeaconIdentifier<P1, P2 where P1: Hashable, P2: Hashable>: Hashable {
    var major: P1
    var minor: P2
    init(major: P1, minor: P2) {
        self.major = major
        self.minor = minor
    }

    var hashValue: Int {
        get {
            return (31 &* major.hashValue) &+ minor.hashValue
        }
    }
}

func == <T1, T2, U1, U2> (lhs: BeaconIdentifier<T1, T2>, rhs: BeaconIdentifier<U1, U2>) -> Bool {
    return (lhs.major.hashValue == rhs.major.hashValue) && (lhs.minor.hashValue == rhs.minor.hashValue)
}

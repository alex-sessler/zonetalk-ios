//
//  ZoneDetailViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 04/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class ZoneDetailViewController: UIViewController {
    
    let persistenceService = PersistenceService.sharedInstance
    
    // are passed through segue
    var zoneId: String!
    var user: User!

    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var zoneTitle: UILabel!
    @IBOutlet weak var zoneDescription: UITextView!
    @IBOutlet weak var zoneMessage: UITextView!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var ownerNameButton: UIButton!
    
    
    override func viewWillAppear(animated: Bool) {

        if let zone = persistenceService.getZone(zoneId) {
            categoryIcon.image = UIImage(named: Constants.getCategoryImageUrlForName(zone.main_category.name, inverse: false))
            zoneTitle.text = zone.title
            if let zoneDescription = zone.desc
            {
                var attributedText = NSMutableAttributedString(string: zoneDescription)
                let length = countElements(zoneDescription) as Int
                
                attributedText.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(14), range: NSRange(location: 0, length: length))
                self.zoneDescription.attributedText = attributedText
                
                /*
                var a = self.zoneDescription.sizeThatFits(CGSizeMake(self.zoneDescription.frame.size.width, CGFloat.max)).height

                var frame: CGRect = self.zoneDescription.frame
                frame.size.height = a

                self.zoneDescription.frame = frame
*/
                
            } else {
                self.zoneDescription.hidden = true
            }
            
           let zoneMessages = zone.messages.allObjects as? Array<ZoneMessage>
            if (zoneMessages != nil && zoneMessages!.count > 0) {
                
                zoneMessages!.sorted({$0.modified_date.compare($1.modified_date) == NSComparisonResult.OrderedAscending})
                
                
                let message = zoneMessages![0]
                self.zoneMessage.text = message.message
                
                var dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"

                let s = dateFormatter.stringFromDate(message.modified_date)
                
                self.lastUpdatedLabel.textColor = UIColor.grayColor()
                self.lastUpdatedLabel.text = "Updated: " + s + " by"
            }
            
            self.ownerNameButton.setTitle(zone.zone_owner.display_name, forState: UIControlState.Normal)
            
            self.user = zone.zone_owner
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ZoneOwnerDetail") {
            let vc = segue.destinationViewController as UserViewController
            
            vc.user = self.user
        }
    }
}

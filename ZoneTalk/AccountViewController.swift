//
//  AccountViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 08/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController, UITextFieldDelegate {
    
    let persistenceService = PersistenceService.sharedInstance
    
    var keyboardTargetOffset = CGFloat(0)
    var user: User? = nil

    @IBOutlet weak var joinedDateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var editButton: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var changePasswordButton: UIButton!

    
    override func viewDidLoad() {
        
        
        if let id = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY) {
            
            if (id == Constants.GUEST_USER_ID) {
                KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_ID_KEY)
                KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
                
                let delegate = UIApplication.sharedApplication().delegate!
                
                if let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginViewNavigationController") as? UINavigationController {
                    delegate.window!?.rootViewController = loginViewController
                }
            }
        }
        
        super.viewDidLoad()
        
        self.emailTextField.delegate = self
        self.nameTextField.delegate = self
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateSucceeded:", name: Constants.NOTIFICATION_UPDATE_USER_SUCCEEDED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateFailed:", name: Constants.NOTIFICATION_UPDATE_USER_FAILED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        if let id = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY) {
            
            self.user = persistenceService.getUser(id)
            
            
            if (self.user != nil){
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                self.joinedDateLabel.text = dateFormatter.stringFromDate(self.user!.joined)
                
                self.nameTextField.text = self.user!.display_name
                
                if (self.user!.status != nil && !self.user!.status!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).isEmpty) {
                    self.statusLabel.text = self.user!.status
                }
                
                self.emailTextField.text = self.user!.email
                
                if (self.user!.image_url != nil) {
                    let url = NSURL(string: self.user!.image_url!)
                    if let data = NSData(contentsOfURL: url!) {
                        userImage.image = UIImage(data:data)
                    } else {
                        userImage.image = UIImage(named: "icon_user_32.png")
                    }
                    
                    
                } else {
                    userImage.image = UIImage(named: "icon_user_32.png")
                }
                
                self.userImage.layer.cornerRadius = self.userImage.frame.height/2
                self.userImage.layer.masksToBounds = true
                
                self.editButton.userInteractionEnabled = true
                self.editButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "editButtonTapped:"))
                
                if (self.user?.type.lowercaseString == "facebook") {
                    self.changePasswordButton.hidden = true
                }
                
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    @IBAction func finishedEditingEmail(sender: AnyObject) {
        if (!(self.user!.email == self.emailTextField.text)) {
            if (self.user!.type == "facebook") {
                var alert = UIAlertController(title: "Confirm", message: "Really change email address from \(self.user!.email) to \(self.emailTextField.text)?", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                    
                    WebService.sharedInstance.changeUserEmail(self.emailTextField.text, password: nil)
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                    self.nameTextField.text = self.user!.display_name
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                var alert = UIAlertController(title: "Confirm", message: "Confirm password to change email address from \(self.user!.email!) to \(self.emailTextField.text)", preferredStyle: UIAlertControllerStyle.Alert)
                
                var passwordField = UITextField()
                alert.addTextFieldWithConfigurationHandler({ (passwordField) -> Void in
                    passwordField.placeholder = "Password"
                    passwordField.secureTextEntry = true
                    
                })
                
                alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Default, handler: {(a: UIAlertAction!) in
                   WebService.sharedInstance.changeUserEmail(self.emailTextField.text, password: (alert.textFields![0] as UITextField).text)
                    
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                    self.emailTextField.text = self.user!.email
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func finishedEditingName(sender: AnyObject) {
        if (!(self.user!.display_name == self.nameTextField.text)) {
           
            var alert = UIAlertController(title: "Confirm", message: "Really change name from \(self.user!.display_name) to \(self.nameTextField.text)?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                WebService.sharedInstance.saveUserInformation(self.nameTextField.text, status: nil, password: nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                self.nameTextField.text = self.user!.display_name
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func logoutTapped(sender: AnyObject) {
        KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_ID_KEY)
        KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
        
        if(self.user!.type.lowercaseString == "facebook") {
            FacebookLoginService.sharedInstance.logout()
        }
        
        
        let delegate = UIApplication.sharedApplication().delegate!
        
        if let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginViewNavigationController") as? UINavigationController {
            delegate.window!?.rootViewController = loginViewController
        }
        
    }

    
    func editButtonTapped(recognizer: UITapGestureRecognizer) {
        self.performSegueWithIdentifier("EditStatus", sender: nil)
    }
    
    func updateSucceeded(notification: NSNotification) {
        var alert = UIAlertController(title: "Success", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(alert, animated: true, completion: nil)
        })
    }
    
    func updateFailed(notification: NSNotification) {
        var alert = UIAlertController(title: "Error", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        dispatch_async(dispatch_get_main_queue(), {
            self.presentViewController(alert, animated: true, completion: nil)
        })
        
        self.nameTextField.text = self.user!.display_name
    }
    
    /* KEYBOARD UTIL */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if (self.keyboardTargetOffset == 0) {
            self.keyboardTargetOffset = self.view.frame.height - textField.frame.origin.y - textField.frame.height - 8
        }
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            
            if (self.keyboardTargetOffset < keyboardSize.height) {
                let move = keyboardSize.height - self.keyboardTargetOffset
                self.view.frame.origin.y -= move
            }
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            let move = keyboardSize.height - self.keyboardTargetOffset
            self.view.frame.origin.y += move
            self.keyboardTargetOffset = CGFloat(0)
        }
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "EditStatus" {
            
            let vc = segue.destinationViewController as StatusViewController
            
            vc.currentStatus = self.statusLabel.text!
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

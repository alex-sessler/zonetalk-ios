//
//  ZoneUpdateService.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 27/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation

class ZoneUpdateService: NSObject {
    
    let webService = WebService.sharedInstance
    let locationService = LocationService.sharedInstance
    
    var timer : NSTimer?
    
    override init() {
        super.init()
    }
    
    func startUpdatingZones() {
        
        if let currentLocation = locationService.currentLocation {
            self.timer = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: "updateZones", userInfo: nil, repeats: true)
            updateZones()
        } else {
             NSNotificationCenter.defaultCenter().addObserver(self, selector: "initialUpdate:", name: Constants.NOTIFICATION_LOCATION_UPDATED, object: nil)
        }
        
       
    }
    
    func initialUpdate(notification: NSNotification) {
        updateZones()
        self.timer = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: "updateZones", userInfo: nil, repeats: true)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
    func updateZones() {
        println("Updating Zones...")

        self.webService.updateZones(self.locationService.currentLocation!.coordinate.latitude, longitude: self.locationService.currentLocation!.coordinate.longitude, kmRadius: 5)
        self.webService.updateBeaconZones()
        
    }
    
    func stopUpdatingZones() {
        if let timer = self.timer {
            timer.invalidate()
        }
    }


}
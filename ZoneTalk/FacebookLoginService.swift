//
//  FacebookLoginService.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 22/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation

class FacebookLoginService: NSObject, FBLoginViewDelegate {
    
    let webService = WebService.sharedInstance
    
    class var sharedInstance: FacebookLoginService {
        struct Singleton {
            static let facebookLoginService = FacebookLoginService()
        }
        return Singleton.facebookLoginService
    }
    
    /* FACEBOOK */
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        
        webService.loginUser("abcd", password: FBSession.activeSession().accessTokenData.accessToken)
        // perform segue
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        println("FB Login Error: \(handleError.localizedDescription)")
    }
    
    func logout() {
        FBSession.activeSession().closeAndClearTokenInformation()
        FBSession.activeSession().close()
    }
    
    
    
}
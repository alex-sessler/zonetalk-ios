//
//  UserViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 04/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    
    // is passed through segue
    var user: User!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var displayName: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        
        if (user.image_url != nil) {
            let url = NSURL(string: user.image_url!)
            let data = NSData(contentsOfURL: url!)
            
            userImage.image = UIImage(data:data!)
        } else {
            userImage.image = UIImage(named: "icon_user_32.png")
        }
        
        self.userImage.layer.cornerRadius = 75
        self.userImage.layer.masksToBounds = true
        self.displayName.text = user.display_name
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        self.joinedLabel.text = "Joined: " + dateFormatter.stringFromDate(user.joined)
        
        self.userIcon.image = UIImage(named: "icon_user_blue.png")
        
        if (user.status != nil && !user.status!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).isEmpty) {
             self.statusLabel.text = user.status!
        }
    }

}

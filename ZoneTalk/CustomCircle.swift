//
//  CustomCircle.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 22/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit
import MapKit

class CustomCircle: MKCircle {
    
    var strokeColor: UIColor?
    var fillColor: UIColor?
    var strokeWidth: CGFloat?
   
}

//
//  ZoneTableViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 10/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import UIKit
import CoreLocation

class ZoneTableViewController: UITableViewController, CLLocationManagerDelegate {

    let locationService = LocationService.sharedInstance
    let persistenceService = PersistenceService.sharedInstance
    
    var zones : Dictionary<String, Array<ZoneTableItem>> = Dictionary<String, Array<ZoneTableItem>>()
    var sections : [(name: String, expanded: Bool)] = []
    var filters: [Filter] = []
    
    @IBOutlet weak var FilterButton: UIBarButtonItem!
    @IBOutlet var zoneTable: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var nib = UINib(nibName: "ZoneTableCell", bundle: nil)
        zoneTable.registerNib(nib, forCellReuseIdentifier: "zoneTableCell")
        
        var nib2 = UINib(nibName: "ZoneTableHeaderCell", bundle: nil)
        zoneTable.registerNib(nib2, forCellReuseIdentifier: "zoneTableHeaderCell")
        

        tableView.tableFooterView = UIView(frame:CGRectZero)
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "zonesFetched:", name: Constants.NOTIFICATION_ZONES_FETCHED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationUpdated:", name: Constants.NOTIFICATION_LOCATION_UPDATED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beaconUpdated:", name: Constants.NOTIFICATION_BEACON_UPDATED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beaconZoneExited:", name: Constants.NOTIFICATION_BEACON_EXITED, object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.filters = self.persistenceService.getFiltersForUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)
        
        // apply filters
        if (self.locationService.currentLocation != nil) {
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {
                if (self.locationService.currentLocation != nil) {
                    self.persistenceService.getZones()
                }
            })
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    
    func locationUpdated(notification: NSNotification) {
        if (self.locationService.currentLocation != nil) {
            self.persistenceService.getZones()
        }
    }
    
    func beaconUpdated(notification: NSNotification) {
        
        let major: NSNumber = notification.userInfo!["major"] as NSNumber
        
        let minor: NSNumber = notification.userInfo!["minor"] as NSNumber
        
        let proximity: Int = notification.userInfo!["proximity"] as Int
        
        var distance: String?
        
        switch proximity {
            case 1: distance = "I"
                break
            case 2: distance = "N"
                break
            case 3: distance = "F"
                break
            default: distance = ""
                break
        }
        
        
        if let zone = persistenceService.getBeaconZone(major, minor: minor) {
            if let category = self.getCategoryToDisplayZoneUnder(zone.main_category, otherCategories: zone.other_categories) {
                self.mergeZoneIntoActiveZones(zone, category: category, distanceFromLocation: distance!)
                dispatch_async(dispatch_get_main_queue(), {
                    self.zoneTable.reloadData()
                })
            }
        }
    }
    
    func beaconZoneExited(notification: NSNotification) {
        
        let major: NSNumber = notification.userInfo!["major"] as NSNumber
        let minor: NSNumber = notification.userInfo!["minor"] as NSNumber
        
        if let zone = persistenceService.getBeaconZone(major, minor: minor) {
            if let category = self.getCategoryToDisplayZoneUnder(zone.main_category, otherCategories: zone.other_categories) {
                self.cleanUpOldEntries(zone, categoryToDisplay: category)
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.zoneTable.reloadData()
        })
    }


    

    /* ZONES */
    func zonesFetched(notification: NSNotification) {
       
        if let existingZones = notification.userInfo!["zones"] as? [Zone] {
            for zone in existingZones {
                
                if (zone.zone_type == "BEACON") {
                    continue
                }
                
                let zonePosition = CLLocation(latitude: zone.latitude.doubleValue, longitude: zone.longitude.doubleValue)
                let currentPosition = self.locationService.currentLocation
                
                if (currentPosition == nil) {
                    return
                }
                
                let distanceFromZone = currentPosition!.distanceFromLocation(zonePosition)
                
                let unfilteredCategoryForZone = self.getCategoryToDisplayZoneUnder(zone.main_category, otherCategories: zone.other_categories)
                
                
                
                if (distanceFromZone <= zone.radius.doubleValue && unfilteredCategoryForZone != nil) {
                    // user is in this zone
                    self.mergeZoneIntoActiveZones(zone, category: unfilteredCategoryForZone! ,distanceFromLocation: String(format: "%.0f", distanceFromZone))
                    
                } else {
                    cleanUpOldEntries(zone, categoryToDisplay: unfilteredCategoryForZone)
                }
            }
        } else {
            // TODO: handle errors
        }

        dispatch_async(dispatch_get_main_queue(), {
            self.zoneTable.reloadData()
        })
    }
    
    func cleanUpOldEntries(zone: Zone, categoryToDisplay: Category?) {
        // iterate through the zones of the target array associated with the category
        // that this zone should be displayed under and try to avoid duplicate entry
        if (categoryToDisplay != nil) {
            if let categoriesArray = self.zones[categoryToDisplay!.name] {
                for (index, z) in enumerate(categoriesArray) {
                    // make sure entry is not inserted as duplicate
                    if (z.id == zone.id) {
                        self.zones[categoryToDisplay!.name]?.removeAtIndex(index)
                        break
                    }
                }
            }
        }
       
        
        // if the zone is displayed under a different category than its main category
        // (because the main category was filtered out), iterate through the main categories'
        // array to remove old (pre-filter) entries
        if (categoryToDisplay == nil || zone.main_category.name != categoryToDisplay!.name) {
            
            if let categoriesArray = self.zones[zone.main_category.name] {
                for (index, z) in enumerate(categoriesArray) {
                    // make sure entry is not inserted as duplicate
                    if (z.id == zone.id) {
                        self.zones[zone.main_category.name]?.removeAtIndex(index)
                        break
                    }
                }
            }
            outerLoop: for otherCategory in zone.other_categories {
                if let categoriesArray = self.zones[otherCategory.name] {
                    for (index, z) in enumerate(categoriesArray) {
                        // make sure entry is not inserted as duplicate
                        if (z.id == zone.id) {
                            self.zones[otherCategory.name]?.removeAtIndex(index)
                            break outerLoop
                        }
                    }
                }
            }
        }
        
        // remove empty categories
        self.removeEmptyCategories()
    }
    
    func mergeZoneIntoActiveZones(zone: Zone, category: Category, distanceFromLocation: String) {
        
        cleanUpOldEntries(zone, categoryToDisplay: category)
        
        if (self.zones[category.name] == nil) {
            // there is no zone for this category yet
            self.zones.updateValue(Array<ZoneTableItem>(), forKey: category.name)
            let category = (name: category.name, expanded: true)
            self.sections.append(category)
            self.sections = self.sections.sorted{$0.name < $1.name}
        }
        
        self.zones[category.name]!.append(ZoneTableItem(id: zone.id, title: zone.title, distance: distanceFromLocation, mainCategory:category.name, ownerId: zone.zone_owner.id))
        
        // make sure the zones are sorted by distance
        if let arr = self.zones[category.name] {
            self.zones[category.name] = arr.sorted({$0.distance < $1.distance})
        }
    }
    
    
    /* TABLE VIEW CONFIG */
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0 && (indexPath.section < sections.count)) {
            let headerCell = self.zoneTable.dequeueReusableCellWithIdentifier("zoneTableHeaderCell") as ZoneTableHeaderCell
            headerCell.backgroundColor = Constants.ZONETALK_COLOR
            var sectionName = sections[indexPath.section].name
            var zonesForSection = zones[sectionName]!
            
            headerCell.loadItem(sectionName, categoryImageUrl: Constants.getCategoryImageUrlForName(sectionName, inverse: false), numberOfZones: String(zonesForSection.count))
            
            // will change maybe
            return headerCell
            
        } else if (indexPath.row > 0) {
            let cell: ZoneTableCell = self.zoneTable.dequeueReusableCellWithIdentifier("zoneTableCell") as ZoneTableCell
            
            var sectionIndex = indexPath.section
            var sectionName = sections[sectionIndex].name
            var items : [ZoneTableItem] = self.zones[sectionName]!
            
            
            // Configure the cell...
            var zone : ZoneTableItem
            zone = items[indexPath.row - 1]
            
            cell.loadItem(zone.title, distance: zone.distance)
            
            return cell
        }
        
        return self.zoneTable.dequeueReusableCellWithIdentifier("zoneTableCell") as ZoneTableCell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        var sec = sections[section]
        if let entriesInSection = self.zones[sec.name] {
            if (sec.expanded) {
                return entriesInSection.count + 1
            } else {
                return 1
            }
        }
        return 0
    }

    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 0) {
            sections[indexPath.section].expanded = !sections[indexPath.section].expanded
            if (sections[indexPath.section].expanded) {
                
            }
            self.zoneTable.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: UITableViewRowAnimation.Fade)
        } else {
            let cell = zoneTable.cellForRowAtIndexPath(indexPath)
            performSegueWithIdentifier("ZoneDetailSegue", sender: cell)
        }
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    
    /* CATEGORIES */
    
    func removeEmptyCategories() {
        
        for category in self.zones.keys.array {
            
            if let zonesForCategory = self.zones[category] {
                
                if (zonesForCategory.isEmpty) {
                    self.zones.removeValueForKey(category)
                    for (index, z) in enumerate(self.sections) {
                        // make sure entry is not inserted as duplicate
                        if (z.name == category) {
                            self.sections.removeAtIndex(index)
                        }
                    }
                }
            }
        }
    }
    
    func getCategoryToDisplayZoneUnder(mainCategory: Category, otherCategories: NSSet) -> Category? {
        
        if (!isCategoryFiltered(mainCategory)) {
            return mainCategory
        }
        
        var categoriesOfZone = otherCategories.allObjects as Array<Category>
        
        var unfilteredCategories = [Category]()
        
        for (index, category) in enumerate(categoriesOfZone) {
            if (!isCategoryFiltered(category)) {
                unfilteredCategories.append(category)
            }
        }

        if (unfilteredCategories.isEmpty) {
            return nil
        }
        
       unfilteredCategories.sort({$0.name < $1.name})
        
        return unfilteredCategories[0]
    }
    
    func isCategoryFiltered(category: Category) -> Bool{
        for filter in self.filters {
            if (filter.active.boolValue && filter.category.name == category.name) {
                return true
            }
        }
        return false
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ZoneDetailSegue" {
            if let selectedIndex = self.zoneTable.indexPathForCell(sender as UITableViewCell) {
                
                
                let vc = segue.destinationViewController as ZoneDetailViewController
                
                let categoryName = sections[selectedIndex.section].name
                let targetZones = zones[categoryName]!
                let targetZone = targetZones[selectedIndex.row - 1]
                
                vc.zoneId = targetZone.id
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

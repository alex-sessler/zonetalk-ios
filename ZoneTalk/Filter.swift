//
//  Filter.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 05/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class Filter: NSManagedObject {

    @NSManaged var active: NSNumber
    @NSManaged var category: ZoneTalk.Category
    @NSManaged var user: ZoneTalk.User

}

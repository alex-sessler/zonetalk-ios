//
//  LoginViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 10/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    let webService : WebService = WebService.sharedInstance
    let fbLoginService = FacebookLoginService.sharedInstance
    
    var activityIndicator: UIActivityIndicatorView?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var fbLoginButton: UIImageView!
    @IBOutlet var fbLoginView: FBLoginView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fbLoginView.delegate = self.fbLoginService
        self.fbLoginView.readPermissions = ["public_profile"]
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.activityIndicator = ZoneTalkActivityIndicator(view: self.view)
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginSucceeded:", name: Constants.NOTIFICATION_LOGIN_SUCCEEDED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginFailed:", name: Constants.NOTIFICATION_LOGIN_FAILED, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func loginButtonTapped(sender: UIButton) {
        
        let email = self.emailTextField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let password = self.passwordTextField.text
        
        self.emailTextField.endEditing(true)
        self.passwordTextField.endEditing(true)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            self.webService.loginUser(email, password: password)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator!.startAnimating()
            })
            
        })
       
    }

    @IBAction func guestButtonTapped(sender: AnyObject) {

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            self.webService.loginUser(Constants.GUEST_USER_EMAIL, password: Constants.GUEST_USER_PASSWORD)
            
            dispatch_async(dispatch_get_main_queue(), {
              self.activityIndicator!.startAnimating()
            })
            
        })
    }
    
    func loginSucceeded(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        
        let delegate = UIApplication.sharedApplication().delegate!
        if let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UIViewController {
            delegate.window!!.rootViewController = initialViewController
        }
        
    }
    
    func loginFailed(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })        
        
        let message = notification.userInfo!["message"] as String
        
        self.infoIcon.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
        self.infoLabel.textColor = UIColor.redColor()
        self.infoLabel.text = (message)
        
        if (message == "Network Error") {
            NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "repeatLogin", userInfo: nil, repeats: false)
        } else {
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
    }
    
    func repeatLogin() {
        println("repeat login")
        self.webService.loginUser()
    }
   
    // make sure the keyboard is hidden when the "done" key is pressed
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if (textField === self.emailTextField && countElements(self.passwordTextField.text) == 0)
        {
            self.passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

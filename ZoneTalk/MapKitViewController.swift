//
//  MapKitViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 22/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit
import MapKit

class MapKitViewController: UIViewController, MKMapViewDelegate, UIPopoverPresentationControllerDelegate {

    let persistenceService = PersistenceService.sharedInstance
    
    var navigationBar : UIView?
    var previewController: MapPreviewViewController?
    var route : MKRoute? = nil
    var foundInitialPosition = false
    var zones = Dictionary<String, [String]>()
    
    var filters : [Filter]?
    
    @IBOutlet var longTapRecognizer: UILongPressGestureRecognizer!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let location = CLLocationCoordinate2D(
            latitude: 50.541271,
            longitude: 10.300052
        )
        // 2
        let span = MKCoordinateSpanMake(10, 10)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.showsPointsOfInterest = false

        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {

        self.filters = PersistenceService.sharedInstance.getFiltersForUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "detailButtonTapped:", name: Constants.NOTIFICATION_PREVIEW_CELL_DETAIL_PRESSED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "navigationButtonTapped:", name: Constants.NOTIFICATION_PREVIEW_CELL_NAVIGATION_PRESSED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "zonesFetched:", name: Constants.NOTIFICATION_ZONES_FETCHED, object: nil)
        
        self.persistenceService.getZones()
    }
    
    override func viewWillDisappear(animated: Bool) {
         NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func zonesFetched(notification: NSNotification) {
        
        self.zones.removeAll(keepCapacity: false)
        self.mapView.removeOverlays(mapView.overlays)
        
        if let zones = notification.userInfo!["zones"] as? [Zone] {
            
            for zone in zones {
                
                let categoryToDisplayZoneUnder = getCategoryToDisplayZoneUnder(zone.main_category, otherCategories: zone.other_categories)
                if (categoryToDisplayZoneUnder != nil && zone.radius.integerValue > 0 && zone.main_category != NSNull()) {
                    drawCircle(zone.latitude.floatValue, longitude: zone.longitude.floatValue, radius: zone.radius.doubleValue, main_category: categoryToDisplayZoneUnder!)
                    if (self.zones[categoryToDisplayZoneUnder!.name] == nil)
                    {
                        self.zones.updateValue([String](), forKey: categoryToDisplayZoneUnder!.name)
                    }
                   self.zones[categoryToDisplayZoneUnder!.name]?.append(zone.id)
                }
            }
        }
        
        if (self.route != nil) {
            self.mapView.addOverlay(self.route?.polyline)
        }
        
    }
    
    func getCategoryToDisplayZoneUnder(mainCategory: Category, otherCategories: NSSet) -> Category? {
        
        if (!isCategoryFiltered(mainCategory)) {
            return mainCategory
        }
        
        var categoriesOfZone = otherCategories.allObjects as Array<Category>
        
        var unfilteredCategories = [Category]()
        
        for (index, category) in enumerate(categoriesOfZone) {
            if (!isCategoryFiltered(category)) {
                unfilteredCategories.append(category)
            }
        }
        
        if (unfilteredCategories.isEmpty) {
            return nil
        }
        
        unfilteredCategories.sort({$0.name < $1.name})
        
        return unfilteredCategories[0]
    }
    
    func isCategoryFiltered(category: Category) -> Bool{
        if let filters = self.filters {
            for filter in filters {
                if (filter.active.boolValue && filter.category.name == category.name) {
                    return true
                }
            }
        }
        return false
    }
    
    @IBAction func tappedLong(sender: AnyObject) {
        
        var currentZones = [ZoneTableItem]()
        
        var touchPoint = self.longTapRecognizer.locationInView(self.mapView)
        var location = self.mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        
        // display preview
        for category in self.zones.keys {
            
            if let zoneIds = self.zones[category] {
                for zoneId in zoneIds {
                    
                    var zone = persistenceService.getZone(zoneId)!
                    
                    let zonePosition = CLLocation(latitude: zone.latitude.doubleValue, longitude: zone.longitude.doubleValue)
                    let distanceFromZone = CLLocation(latitude: location.latitude, longitude: location.longitude).distanceFromLocation(zonePosition)
                    if (distanceFromZone <= zone.radius.doubleValue) {
                        currentZones.append(ZoneTableItem(id: zone.id, title: zone.title, distance: String(format: "%d", distanceFromZone), mainCategory:category, ownerId: zone.zone_owner.id))
                    }
                }
            }
            
        }
        
        self.previewController = MapPreviewViewController()
        self.previewController!.zones = currentZones
        self.previewController!.modalPresentationStyle = .Popover
        self.previewController!.preferredContentSize = CGSizeMake(300, CGFloat(44*countElements(currentZones)))
        
        let popoverController = self.previewController!.popoverPresentationController
        
        popoverController?.permittedArrowDirections = .Any
        popoverController?.delegate = self
        popoverController?.sourceView = self.mapView
        popoverController?.sourceRect = CGRect(
            x: touchPoint.x,
            y: touchPoint.y,
            width: 1,
            height: 1)
        
        self.presentViewController(self.previewController!,
            animated: true,
            completion: nil)
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController!) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .None
    }
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        
        if (!foundInitialPosition) {
            let location = CLLocationCoordinate2D(
                latitude: userLocation.location.coordinate.latitude,
                longitude: userLocation.location.coordinate.longitude
            )
            
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView.setRegion(region, animated: true)
            foundInitialPosition = true
        }
    }
    

    
    func drawCircle(latitude: Float, longitude: Float, radius: Double, main_category: Category) {
        
        var circleCenter: CLLocationCoordinate2D  = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),longitude: CLLocationDegrees(longitude))
        
        var colors = Constants.getColorForCategory(main_category.name)
        
        var circle = CustomCircle(centerCoordinate: circleCenter, radius: radius)
        circle.strokeColor = colors.regularColor
        circle.fillColor = colors.lightColor
        circle.strokeWidth = 1
        self.mapView.addOverlay(circle)

    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        
        if (overlay.isKindOfClass(CustomCircle)) {
            
            let circle = overlay as CustomCircle
            
            let circleView = MKCircleRenderer(overlay: overlay)
            
            circleView.strokeColor = circle.strokeColor!
            circleView.fillColor = circle.fillColor!
            
            circleView.lineWidth = circle.strokeWidth!
            
            
            return circleView
        } else if (overlay.isKindOfClass(MKPolyline)) {
            let polyline = overlay as MKPolyline
            
            var renderer = MKPolylineRenderer(polyline: polyline)
            renderer.strokeColor = UIColor.redColor()
            renderer.lineWidth = 3
            return renderer
        }
        return nil

    }
    
    func detailButtonTapped(notification: NSNotification) {
  
        performSegueWithIdentifier("ShowDetailForZonePreview", sender: notification.userInfo?["zoneId"])
    }
    
    func navigationButtonTapped(notification: NSNotification) {
        
        clearRoute()
        
        let zoneId = notification.userInfo?["zoneId"] as String
        if let zone = persistenceService.getZone(zoneId) {
            let zoneLocation = CLLocationCoordinate2D(latitude: zone.latitude.doubleValue, longitude: zone.longitude.doubleValue)
            let currentLocation = self.mapView.userLocation.coordinate
            
            
            
            var alert = UIAlertController(title: "Navigation", message: "Navigate to Zone \(zone.title)?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Drive", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                self.getDirections(currentLocation, destination: zoneLocation, transportType: MKDirectionsTransportType.Automobile)
            }))
            alert.addAction(UIAlertAction(title: "Walk", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                self.getDirections(currentLocation, destination: zoneLocation, transportType: MKDirectionsTransportType.Walking)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in
                return
            }))
            
            
            self.dismissViewControllerAnimated(false, completion: {() -> Void in
                self.presentViewController(alert, animated: true, completion: nil)
            })
            
        }
    }
    
    
    func getDirections(start: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, transportType: MKDirectionsTransportType) {
        
        let startMark = MKPlacemark(coordinate: start, addressDictionary: nil)
        let destinationMark = MKPlacemark(coordinate: destination, addressDictionary: nil)
        
        let directionsRequest = MKDirectionsRequest()
        directionsRequest.setSource(MKMapItem(placemark: startMark))
        directionsRequest.setDestination(MKMapItem(placemark: destinationMark))
        directionsRequest.transportType = transportType
        
        
        var directions = MKDirections(request: directionsRequest)
        
        directions.calculateDirectionsWithCompletionHandler { (response:MKDirectionsResponse!, error: NSError!) -> Void in
            if (error == nil) {
                self.route = response.routes[0] as? MKRoute
                self.clearRoute()
                self.showNavigationBar()
                self.mapView.addOverlay(self.route?.polyline)
            }
        }
    }
    
    func showNavigationBar() {

        self.navigationBar = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 60))
        self.navigationBar!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        
        let dismissNavigationButton = UIImageView(frame: CGRectMake(self.view.frame.width - 50, 10, 40, 40))
        dismissNavigationButton.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
        dismissNavigationButton.backgroundColor = UIColor.whiteColor()
        dismissNavigationButton.layer.cornerRadius = dismissNavigationButton.frame.height/2
        dismissNavigationButton.layer.masksToBounds = true
        
        dismissNavigationButton.userInteractionEnabled = true
        dismissNavigationButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "clearRoute:"))
        
        self.navigationBar!.addSubview(dismissNavigationButton)
        
        var label = UILabel(frame: CGRectMake(0, 0, self.view.bounds.width, 21))
        label.center = CGPointMake((self.view.bounds.width/2), (self.navigationBar!.frame.height/2))
        label.textAlignment = NSTextAlignment.Center
        label.font = label.font.fontWithSize(20)
        label.textColor = UIColor.whiteColor()
        
        let formatter = NSDateComponentsFormatter()
        formatter.unitsStyle = NSDateComponentsFormatterUnitsStyle.Abbreviated
        
        let travelTime = formatter.stringFromTimeInterval(self.route!.expectedTravelTime)
        label.text = travelTime
        
        self.navigationBar!.addSubview(label)
        
        self.view.addSubview(self.navigationBar!)
        
        UIView.animateWithDuration(0.2, animations: {
            var yPosition = self.tabBarController!.tabBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height - 5
            yPosition = self.mapView.frame.origin.y
            
            self.navigationBar!.frame = CGRectMake(0, yPosition, self.view.frame.width, 60)
        })
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShowDetailForZonePreview") {
            if let preview = self.previewController {
                self.dismissViewControllerAnimated(false, completion: nil)
                self.previewController = nil
            }
            
            let vc = segue.destinationViewController as ZoneDetailViewController
            
            vc.zoneId = (sender as String)
        }
    }
    
    
    
    func clearRoute(recognizer: UITapGestureRecognizer) {
        clearRoute()
    }
    
    func clearRoute() {
        if (self.navigationBar != nil) {
            UIView.animateWithDuration(0.2, animations: {
                self.navigationBar!.frame = CGRectMake(0, 0, self.view.frame.width, 60)
            }, completion: { (Bool) -> Void in
                self.navigationBar!.removeFromSuperview()
                self.navigationBar = nil
            })
        }
        
        var overlays = [AnyObject]()
        
        for overlay in self.mapView.overlays {
            if (overlay.isKindOfClass(MKPolyline)) {
                overlays.append(overlay)
            }
        }
        
        self.mapView.removeOverlays(overlays)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  Constants.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 28/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation

public struct Constants {
    
    static let GMAPS_API_KEY = "AIzaSyAGruG3JRPytuqxmnpMFRbAEoRVFOfzvB4"
    
    static let KEYCHAIN_USER_ID_KEY = "zonetalk_user_id"
    static let KEYCHAIN_USER_SECRET_KEY = "zonetalk_user_secret"
    
    static let GUEST_USER_ID = "b79b21db-86d1-4c28-a7bb-617734c9073b"
    static let GUEST_USER_EMAIL = "guest@guest.de"
    static let GUEST_USER_PASSWORD = "guest"
    
    /* WEBSERVICE URLS */
    static let WEBSERVICE_URL = "http://zonetalk.de/api"
   
    static let URL_USER = "/user/me"
    static let URL_GET_ALL_ZONES = WEBSERVICE_URL + "/zone"
    static let URL_SIGN_UP = "/user/register"
    static let URL_NEAREST = "/zone/nearest"
    static let URL_BEACONS = "/zone/type/BEACON"
    
    static func getUrlGetMessageForZone(id: String) -> String {
        return WEBSERVICE_URL + "/zone/" + id + "/message"
    }
    
    static func getUrlUser() -> String {
        return WEBSERVICE_URL + URL_USER
    }
    
    static func getUrlSignUp() -> String {
        return WEBSERVICE_URL + URL_SIGN_UP
    }
    
    static func getUrlGetZonesWithinRadius(latitude: Double, longitude: Double, radius: Int) -> String {
        let latitudeString = String(format:"%f",latitude)
        let longitudeString = String(format:"%f",longitude)
        return WEBSERVICE_URL + URL_NEAREST + "/\(latitudeString)/\(longitudeString)/\(radius)"
    }
    
    static func getUrlBeaconZones() -> String {
        return WEBSERVICE_URL + URL_BEACONS
    }

    
    /* COLORS */
    static let BLUE: UIColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.6)
    static let BLUE_LIGHT: UIColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.25)
    
    static let PURPLE: UIColor = UIColor(red: 0.5, green: 0, blue: 0.5, alpha: 0.6)
    static let PURPLE_LIGHT: UIColor = UIColor(red: 0.5, green: 0, blue: 0.5, alpha: 0.25)
    
    static let MAGENTA: UIColor = UIColor(red: 1, green: 0, blue: 1, alpha: 0.6)
    static let MAGENTA_LIGHT: UIColor = UIColor(red: 1, green: 0, blue: 1, alpha: 0.25)
   
    static let RED: UIColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.6)
    static let RED_LIGHT: UIColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.25)
    
    static let ORANGE: UIColor = UIColor(red: 1, green: 0.65, blue: 0, alpha: 0.6)
    static let ORANGE_LIGHT: UIColor = UIColor(red: 1, green: 0.65, blue: 0, alpha: 0.25)
   
    static let YELLOW: UIColor = UIColor(red: 1, green: 1, blue: 0, alpha: 0.6)
    static let YELLOW_LIGHT: UIColor = UIColor(red: 1, green: 1, blue: 0, alpha: 0.25)
   
    static let GREEN: UIColor = UIColor(red: 0, green: 1, blue: 0, alpha: 0.6)
    static let GREEN_LIGHT: UIColor = UIColor(red: 0, green: 1, blue: 0, alpha: 0.25)
    
    static let TURQUOISE: UIColor = UIColor(red: 0.25, green: 0.88, blue: 0.82, alpha: 0.6)
    static let TURQUOISE_LIGHT: UIColor = UIColor(red: 0.25, green: 0.88, blue: 0.82, alpha: 0.25)
    
    // #00b9d2
    static let ZONETALK_COLOR: UIColor = UIColor(red: 0, green: 0.725, blue: 0.824, alpha: 1)
    
    // #12fd3b (green)
    static let ZONETALK_COLOR_GREEN: UIColor = UIColor(red: 0.071, green: 0.992, blue: 0.231, alpha: 1)
    
    // ff2312 (red)
    static let ZONETALK_COLOR_RED: UIColor = UIColor(red: 1, green: 0.137, blue: 0.071, alpha: 1)
    
    static func getColorForCategory(categoryName: String) -> (regularColor: UIColor, lightColor: UIColor){
        switch categoryName {
            
            case "GASTRONOMY":
                return (Constants.ORANGE, Constants.ORANGE_LIGHT)
            case "SHOPPING":
                return (Constants.YELLOW, Constants.YELLOW_LIGHT)
            case "TOURISM":
                return (Constants.PURPLE, Constants.PURPLE_LIGHT)
            case "CITY":
                return (Constants.BLUE, Constants.BLUE_LIGHT)
            case "SOCIAL":
                return (Constants.GREEN, Constants.GREEN_LIGHT)
            case "TRANSPORT":
                return (Constants.TURQUOISE, Constants.TURQUOISE_LIGHT)
            case "OTHERS":
                return (Constants.MAGENTA, Constants.MAGENTA_LIGHT)
            default:
                return (UIColor(red: 1, green: 1, blue: 1, alpha: 0.6), UIColor(red: 0, green: 0, blue: 0, alpha: 0.25))
        }

    }
    
    /* IMAGES */
    static let IMAGE_ICON_ERROR = "close-50.png"
    
    static let IMAGE_CITY  = "categories_city.png"
    static let IMAGE_GASTRONOMY  = "categories_gastronomy.png"
    static let IMAGE_SHOPPING  = "categories_shopping.png"
    static let IMAGE_TOURISM  = "categories_tourism.png"
    static let IMAGE_SOCIAL  = "categories_social.png"
    static let IMAGE_TRANSPORT  = "categories_transport.png"
    static let IMAGE_OTHER  = "categories_other.png"
    
    static let IMAGE_CITY_INVERSE  = "categories_city_inverse.png"
    static let IMAGE_GASTRONOMY_INVERSE  = "categories_gastronomy_inverse.png"
    static let IMAGE_SHOPPING_INVERSE  = "categories_shopping_inverse.png"
    static let IMAGE_TOURISM_INVERSE  = "categories_tourism_inverse.png"
    static let IMAGE_SOCIAL_INVERSE  = "categories_social_inverse.png"
    static let IMAGE_TRANSPORT_INVERSE  = "categories_transport_inverse.png"
    static let IMAGE_OTHER_INVERSE  = "categories_other_inverse.png"
    
    static let IMAGE_SIGNAL_LOW = "low_connection-50.png"
    static let IMAGE_SIGNAL_MEDIUM = "medium_connection-50.png"
    static let IMAGE_SIGNAL_HIGH = "high_connection-50.png"
    
    
    static func getCategoryImageUrlForName(categoryName: String, inverse: Bool) -> String {
        switch categoryName {
            
            case "GASTRONOMY":
                if (inverse) {
                    return IMAGE_GASTRONOMY_INVERSE
                } else {
                    return IMAGE_GASTRONOMY
                }
            case "SHOPPING":
                if (inverse) {
                    return IMAGE_SHOPPING_INVERSE
                } else {
                    return IMAGE_SHOPPING
                }
            case "TOURISM":
                if (inverse) {
                    return IMAGE_TOURISM_INVERSE
                } else {
                    return IMAGE_TOURISM
                }
            case "CITY":
                if (inverse) {
                    return IMAGE_CITY_INVERSE
                } else {
                    return IMAGE_CITY
                }
            case "SOCIAL":
                if (inverse) {
                    return IMAGE_SOCIAL_INVERSE
                } else {
                    return IMAGE_SOCIAL
                }
            case "TRANSPORT":
                if (inverse) {
                    return IMAGE_TRANSPORT_INVERSE
                } else {
                    return IMAGE_TRANSPORT
                }
            case "OTHERS":
                if (inverse) {
                    return IMAGE_OTHER_INVERSE
                } else {
                    return IMAGE_OTHER
                }
            default:
                if (inverse) {
                    return IMAGE_OTHER_INVERSE
                } else {
                    return IMAGE_OTHER
            }
        }
    }
    
    static func getImageNameForSignal(strength: Int) -> String {
        switch (strength) {
            case 1: return IMAGE_SIGNAL_LOW
            case 2: return IMAGE_SIGNAL_MEDIUM
            case 3: return IMAGE_SIGNAL_HIGH
            default: return IMAGE_SIGNAL_LOW
        }
    }
    
    /* NOTIFICATIONS */
    static let NOTIFICATION_LOGIN_SUCCEEDED = "notification_login_succeeded"    
    static let NOTIFICATION_LOGIN_FAILED = "notification_login_failed"
    static let NOTIFICATION_SIGN_UP_SUCCEEDED = "notification_sign_up_succeeded"
    static let NOTIFICATION_SIGN_UP_FAILED = "notification_sign_up_failed"
    static let NOTIFICATION_UPDATE_USER_SUCCEEDED = "notification_update_user_succeeded"
    static let NOTIFICATION_UPDATE_USER_FAILED = "notification_update_user_failed"
    static let NOTIFICATION_ZONES_TABLE_DATA_UPDATED = "notification_zones_table_data_updated"
    static let NOTIFICATION_ZONES_FETCHED = "notification_zones_fetched"
    static let NOTIFICATION_PREVIEW_CELL_DETAIL_PRESSED = "notification_preview_cell_detail_pressed"
    static let NOTIFICATION_PREVIEW_CELL_NAVIGATION_PRESSED = "notification_preview_cell_navigation_pressed"
    static let NOTIFICATION_LOCATION_UPDATED = "notification_location_updated"
    static let NOTIFICATION_BEACON_UPDATED = "notification_beacon_updated"
    static let NOTIFICATION_BEACON_EXITED = "notification_beacon_exited"
    
    
    
    
    

    
}
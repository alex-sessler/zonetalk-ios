//
//  PasswordViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 13/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController {
    
    let webService: WebService = WebService.sharedInstance
    
    var activityIndicator: UIActivityIndicatorView?

    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator = ZoneTalkActivityIndicator(view: self.view)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginSucceeded:", name: Constants.NOTIFICATION_LOGIN_SUCCEEDED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginFailed:", name: Constants.NOTIFICATION_LOGIN_FAILED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePasswordSucceeded:", name: Constants.NOTIFICATION_UPDATE_USER_SUCCEEDED, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    @IBAction func savePasswordTapped(sender: AnyObject) {
        if (!(newPasswordTextField.text == repeatPasswordTextField.text)) {
            self.infoIcon.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
            self.infoLabel.textColor = UIColor.redColor()
            self.infoLabel.text = ("New Passwords don't match")
            
            self.newPasswordTextField.text = ""
            self.repeatPasswordTextField.text = ""
            
            // show inequal passwords
            return
        }
        
        
        let user = PersistenceService.sharedInstance.getUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)
        
        WebService.sharedInstance.loginUser(user!.email!, password: oldPasswordTextField.text)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator!.startAnimating()
            })
            
        })
    }
    
    func loginSucceeded(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
                
                self.webService.saveUserInformation(nil, status: nil, password: self.newPasswordTextField.text)
            })
        })
    }
    
    func updatePasswordSucceeded(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        self.oldPasswordTextField.text = self.newPasswordTextField.text
        self.newPasswordTextField.text = ""
        self.repeatPasswordTextField.text = ""
        
        self.infoIcon.image = UIImage(named: "checkmark-32.png")
        self.infoLabel.text = "Successfully changed"
    }
    
    func loginFailed(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        
        self.infoIcon.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
        self.infoLabel.textColor = UIColor.redColor()
        self.infoLabel.text = ("Incorrect old password")
        
        self.oldPasswordTextField.text = ""
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//
//  User.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 19/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {

    @NSManaged var display_name: String
    @NSManaged var email: String?
    @NSManaged var id: String
    @NSManaged var image_url: String?
    @NSManaged var joined: NSDate
    @NSManaged var modified: NSDate
    @NSManaged var role: NSNumber
    @NSManaged var status: String?
    @NSManaged var type: String
    @NSManaged var zones: NSSet
    @NSManaged var filters: NSSet

}

extension User {
    func populateAttributes(attributes: NSDictionary) {
        
        self.display_name = attributes["displayName"] as String!
        
        if let email = attributes["email"] as? String {
            self.email = email
        }
            
        else {
            self.email = nil
        }
        
        if let imageUrl = attributes["imageUrl"] as? String {
            self.image_url = imageUrl
        } else {
            self.image_url = nil
        }
        
        var joined = attributes["joined"] as NSTimeInterval!
        self.joined =  NSDate(timeIntervalSince1970: (joined/1000))
        
        var modified = attributes["modified"] as NSTimeInterval!
        self.modified =  NSDate(timeIntervalSince1970: (modified/1000))
        
        if let role = attributes["role"] as? NSNumber {
            self.role = role
        }
        
        if let status = attributes["status"] as? String {
            self.status = status
        } else {
            self.status = nil
        }
        
        self.type = attributes["type"] as String!
    }
    
    func addZone(zone: Zone) {
        var zones = self.mutableSetValueForKey("zones")
        zones.addObject(zone)
    }
    
    func setZones(newZones: [Zone]) {
        var zones = self.mutableSetValueForKey("zones")
        zones.removeAllObjects()
        zones.addObjectsFromArray(newZones)
    }
    
    func addFilter(filter: Filter) {
        var filters = self.mutableSetValueForKey("filters")
        filters.addObject(filter)
    }
    
    func setFilters(newFilters: [Filter]) {
        var filters = self.mutableSetValueForKey("filters")
        filters.removeAllObjects()
        filters.addObjectsFromArray(newFilters)
    }
}
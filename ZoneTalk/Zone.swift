//
//  ZoneTalk.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 29/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class Zone: NSManagedObject {

    @NSManaged var city: String?
    @NSManaged var created_date: NSDate
    @NSManaged var desc: String?
    @NSManaged var end_date: NSDate?
    @NSManaged var id: String
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged var modified_date: NSDate
    @NSManaged var radius: NSNumber
    @NSManaged var start_date: NSDate?
    @NSManaged var title: String
    @NSManaged var trusted: NSNumber
    @NSManaged var visited: NSNumber
    @NSManaged var major: NSNumber?
    @NSManaged var minor: NSNumber?
    @NSManaged var zone_type: String
    @NSManaged var other_categories: NSSet
    @NSManaged var messages: NSSet
    @NSManaged var zone_owner: ZoneTalk.User
    @NSManaged var main_category: ZoneTalk.Category
    

}

extension Zone {
    func addOtherCategory(category: Category) {
        var otherCategories = self.mutableSetValueForKey("other_categories")
        otherCategories.addObject(category)
    }
    
    func setOtherCategories(categories: [Category]) {
        var otherCategories = self.mutableSetValueForKey("other_categories")
        otherCategories.removeAllObjects()
        otherCategories.addObjectsFromArray(categories)
    }
}

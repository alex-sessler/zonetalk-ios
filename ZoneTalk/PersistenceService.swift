//
//  PersistenceService.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 20/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class PersistenceService {

    class var sharedInstance: PersistenceService {
        struct Singleton {
            static let persistenceService = PersistenceService()
        }
        return Singleton.persistenceService
    }
    
    let coreDataStack = ZoneTalkCoreDataStack.sharedInstance
    let dateFormatter = NSDateFormatter()

    
    func persistZones(zones: NSArray) {
        var error:NSError? = nil
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH-mm-ss'Z'"
        
        // Persist Users
        var zoneIds = [String]()
        var usersToPersist = Dictionary<String, NSDictionary>()
        var categories = Dictionary<String, Category?>()
        
        let context = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        context.persistentStoreCoordinator = coreDataStack.managedObjectContext?.persistentStoreCoordinator
        
        context.performBlock({
        
            for zone in zones {
                
                var zone_id: String?
                
                var tmp = zone["id"] as? NSNumber
                
                if(tmp != nil) {
                    zone_id = String(tmp as Int)
                } else {
                    zone_id = zone["id"] as? String
                }

                zoneIds.append(zone_id!)
            
                let user = zone["zoneOwner"] as NSDictionary
                let user_id = user["id"] as NSString
                
                usersToPersist.updateValue(user, forKey: user_id)
                
                // get all category names, so the existing ones can be associated to the 
                // Managed objects and the new ones persisted
                if let currentZoneCategories = zone["categories"] as? Array<String> {
                    for category in currentZoneCategories {
                        categories.updateValue(nil, forKey: category)
                    }
                }
            }

            let persistedCategories : Dictionary<String, Category> = self.persistCategories(categories, context: context)
            
            // persist all users/zoneOwners that were found, get reference so the relationship can be stored when storing the zones
            let persistedUsers :Array<User> = self.persistUsers(usersToPersist, context: context)
            
            // fetch all existing zones that need to be updated at once to save ressouces
            let existingZones : Dictionary<String, Zone> = self.getExistingZones(zoneIds, context: context)
            
            
        
            for zone in zones {
                
                var zoneOwner: User? = nil
                
                var zone_id = self.getIdAsString(zone["id"])
     
                if let id = zone_id {
                    
                    var zoneOwnerTmp = zone["zoneOwner"] as NSDictionary
                    
                    for user in persistedUsers {
                        if(zoneOwnerTmp["id"] as String == user.id) {
                            zoneOwner = user
                            break
                        }
                    }
                    if (zoneOwner == nil) {
                        println("No owner found for zone \(id)")
                        continue
                    }
                    
                    if let existingZone = existingZones[id] {
                        
                        let populatedZone = self.populateZoneAttributes(existingZone, zoneDictionary: zone as Dictionary<String, AnyObject>)
                        
                        if (populatedZone == nil) {
                            println("Error populating zone \(id) with attributes \(zone)")
                            continue
                        }
                        populatedZone!.zone_owner = zoneOwner!
                        
                        if let categories = zone["categories"] as? Array<String> {
            
                            populatedZone!.main_category = persistedCategories[categories[0]]!
                        
                            var otherCategories = [Category]()
                            
                            for var i = 1; i < categories.count; i++ {
                                otherCategories.append(persistedCategories[categories[i]]!)
                            }
                            
                            populatedZone!.setOtherCategories(otherCategories)
                        }
                        
                    } else {
                        
                        // no  existing Zone found
                        let newZone = NSEntityDescription.insertNewObjectForEntityForName("Zone", inManagedObjectContext: context) as Zone
                        
                        newZone.id = id
                        
                        let populatedZone = self.populateZoneAttributes(newZone, zoneDictionary: zone as Dictionary<String, AnyObject>)
                        
                        if (populatedZone == nil) {
                            println("Error populating zone \(id) with attributes \(zone)")
                            continue
                        }
                        
                        populatedZone!.zone_owner = zoneOwner!
                        
                        if let categories = zone["categories"] as? Array<String> {
                            
                            populatedZone!.main_category = persistedCategories[categories[0]]!
                            
                            for (var i = 1; i < categories.count; i++) {
                                populatedZone!.addOtherCategory(persistedCategories[categories[i]]!)
                            }
                        }
                    }
                 
                }
            }
                
            context.save(&error)
                
            if (error != nil) {
                println("error saving background context: \(error)")
            }
            
            error = nil
            self.coreDataStack.managedObjectContext?.performBlock({
                self.coreDataStack.managedObjectContext?.save(&error)
                if (error != nil) {
                    println("error saving main context: \(error)")
                }
            })
            println("Zones Persisted...")
        })
        
    }
    
    func getIdAsString(id: AnyObject?) -> String? {
        
        var result_id: String?
        
        var tmp = id as? NSNumber
        
        if(tmp != nil) {
            result_id = String(tmp as Int)
        } else {
            result_id = id as? String
        }
        
        return result_id
    }
    
    func getExistingZones(ids: Array<String>, context: NSManagedObjectContext) -> Dictionary<String, Zone> {
        var result = Dictionary<String, Zone>()
        
        let fetchRequest = NSFetchRequest(entityName: "Zone")
        
        let predicate = NSPredicate(format: "id in %@", ids)
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var error:NSError? = nil
        
        let fetchResult: [Zone]? = context.executeFetchRequest(fetchRequest, error: &error) as? [Zone]
        
        if let fetchedZones = fetchResult? {
            
            for existingZone in fetchedZones {
                result.updateValue(existingZone, forKey: existingZone.id)
            }
        }
        
        return result
    }
    
    func populateZoneAttributes(newZone: Zone, zoneDictionary: Dictionary<String, AnyObject?>) -> Zone? {
        
        if let city = zoneDictionary["city"] as? String {
            newZone.city = city
        } else {
            newZone.city = nil
        }
        
        newZone.created_date = dateFormatter.dateFromString(zoneDictionary["created"] as String)!
        
        var desc = zoneDictionary["description"] as? String
        
        if (desc != nil && desc != NSNull()) {
            newZone.desc = desc
        } else {
            newZone.desc = nil
        }
        
        if let end_date = zoneDictionary["endDate"] as? NSDate {
            newZone.end_date = end_date
        } else {
            newZone.end_date = nil
        }
        
        if let location = zoneDictionary["location"] as? NSArray {
            newZone.latitude = location[0] as Float
            newZone.longitude = location[1] as Float
        } else {
            return nil
        }
        
        newZone.modified_date = dateFormatter.dateFromString(zoneDictionary["modified"] as String)!
        
        newZone.radius = zoneDictionary["radius"] as NSNumber
        
        if let start_date = zoneDictionary["startDate"] as? NSDate {
            newZone.start_date = start_date
        } else {
            newZone.start_date = nil
        }
        
        newZone.title = zoneDictionary["title"] as String
        
        newZone.trusted = zoneDictionary["trusted"] as Bool
        
        newZone.zone_type = zoneDictionary["zoneType"] as String
        
        if (newZone.zone_type == "BEACON") {
            
            newZone.major = zoneDictionary["major"] as? NSNumber
            newZone.minor = zoneDictionary["minor"] as? NSNumber
            println("beacon zone major \(newZone.major) minor \(newZone.minor)")
        }        
  
        return newZone
    }
    
    
    func persistUsers (users: Dictionary<String, NSDictionary>, context: NSManagedObjectContext) -> Array<User> {
        
        var persistedUsers = [User]()
        var userIds = [String](users.keys)
        var error: NSError? = nil
        
        var sortedUserIds = userIds.sorted { $0.localizedCaseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending }
        
        
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        let predicate = NSPredicate(format: "id in %@", sortedUserIds)
        fetchRequest.predicate = predicate
    
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let result: [User]? = context.executeFetchRequest(fetchRequest, error: &error) as? [User]
        
        if let fetchedUsers = result? {
            
            for existingUser in fetchedUsers {
               
                let id = existingUser.id
                
                let newValues = users[existingUser.id]!
                
                existingUser.populateAttributes(newValues)
                
                persistedUsers.append(existingUser)

            }
        }
        
        persistedUsers.sort({$0.id < $1.id})
        
        for id in sortedUserIds {
            var alreadyPersisted = false
            for user in persistedUsers {
                if (id == user.id) {
                    alreadyPersisted = true
                    break
                }
            }
            if (!alreadyPersisted) {
                
                let newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) as User
                
                let newValues = users[id]!
                
                newUser.id = id
                
                newUser.populateAttributes(newValues)
                
                persistedUsers.append(newUser)
                
            }
        }
        
        error = nil
        context.save(&error)
        if (error != nil) {
            println("PersistUsers: error saving background context: \(error)")
        }
        
        error = nil
        self.coreDataStack.managedObjectContext?.performBlock({
            self.coreDataStack.managedObjectContext?.save(&error)
            if (error != nil) {
                println("PersistUsers: error saving main context: \(error)")
            }
        })
        
        return persistedUsers
    }
    
    func persistCategories(categoryStrings: Dictionary<String, Category?>, context: NSManagedObjectContext) -> Dictionary<String, Category> {

        var categories = categoryStrings
        
        var categoryNames = [String](categories.keys)
        
        let fetchRequest = NSFetchRequest(entityName: "Category")
        
        let predicate = NSPredicate(format: "name in %@", categoryNames)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        let result: [Category]? = context.executeFetchRequest(fetchRequest, error: &error) as? [Category]
        
        if let fetchedCategories = result {
            for resultCategory in fetchedCategories {
                categories.updateValue(resultCategory, forKey: resultCategory.name)
            }
        }
        
        for categoryName in categoryNames {
            if (categories[categoryName]! == nil) {
                let newCategory = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: context) as Category
                newCategory.name = categoryName
                categories.updateValue(newCategory, forKey: categoryName)
            }
        }
        
        var resultCategories = Dictionary<String,Category>()
        for category in categories.keys {
            if let categoryObject = categories[category]! {
                resultCategories.updateValue(categoryObject, forKey: category)
            }
        }
        
        error = nil
        context.save(&error)
        if (error != nil) {
            println("Error persisting categories \(error)")
        }
        
        error = nil
        self.coreDataStack.managedObjectContext?.performBlock({
            self.coreDataStack.managedObjectContext?.save(&error)
            if (error != nil) {
                println("PersistCategories: error saving main context: \(error)")
            }
        })
        
        return resultCategories
    }
    
    func getZones() {
        let fetchRequest = NSFetchRequest(entityName: "Zone")
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var error:NSError? = nil
        
        let asyncFetch = NSAsynchronousFetchRequest(fetchRequest: fetchRequest, {(result:NSAsynchronousFetchResult!) -> Void in
            if (error != nil) {
                println("Error fetching zones: \(error)")
            } else {
               
                dispatch_async(dispatch_get_main_queue(), {
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_ZONES_FETCHED, object: nil, userInfo: ["zones": result.finalResult as [Zone]])
                })
            }
        })
        self.coreDataStack.managedObjectContext!.executeRequest(asyncFetch, error: &error)
        if (error != nil) {
            //handle error
        }
        
        let tmpContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        tmpContext.persistentStoreCoordinator = self.coreDataStack.managedObjectContext!.persistentStoreCoordinator
        tmpContext.performBlock({
            
        })
        
    }
    
    func getZone(id: String) -> Zone? {
        let fetchRequest = NSFetchRequest(entityName: "Zone")
        
        let predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        if let result = self.coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [Zone] {
            if (!result.isEmpty) {
                return result[0]
            }
        }
        
        return nil
    }
    
    func saveZoneMessage(messageData: NSDictionary, zoneId: String) {
        
        let id = messageData["id"] as? String
        
        let created_date = dateFormatter.dateFromString(messageData["created"] as String)
        
        let modified_date = dateFormatter.dateFromString(messageData["modified"] as String)
        
        let message = messageData["message"] as? String
        
        
        if (id == nil || created_date == nil || modified_date == nil || message == nil) {
            println("invalid data of zoneMessage Data: \(messageData)")
            return
        }
        
        var zoneMessage : ZoneMessage? = getZoneMessage(id!)
        
        if (zoneMessage == nil) {
            zoneMessage = NSEntityDescription.insertNewObjectForEntityForName("ZoneMessage", inManagedObjectContext: self.coreDataStack.managedObjectContext!) as? ZoneMessage
            zoneMessage!.id = id!
        }
        
        zoneMessage!.created_date = created_date!
        zoneMessage!.modified_date = modified_date!
        zoneMessage!.message = message!
        
        if let zone = getZone(zoneId) {
            var error: NSError?
            
            zoneMessage!.belongs_to = zone
            
            
            
            if (error != nil) {
                println("Error saving zone message \(id) with error \(error!.description)")
            }
            
            var needToInsert = true
            for existingMessage in zone.messages.allObjects as [ZoneMessage] {
                if (existingMessage.id == id) {
                    needToInsert = false
                }
            }
            if (needToInsert) {
                var existingMessages = zone.messages.allObjects as [ZoneMessage]
                existingMessages.append(zoneMessage!)
                zone.messages = NSSet(array:existingMessages)
                
                if (error != nil) {
                    println("Error saving zone message \(id) to zone \(zone.id) with error \(error!.description)")
                }
                
            }
            
            self.coreDataStack.managedObjectContext!.save(&error)
            if (error != nil) {
                println("Error saving zone message \(error)")
            }
                
        } else {
            println("found no zone for message id \(id)")
        }
    }
    
    func getZoneMessage(id: String) -> ZoneMessage? {
        let fetchRequest = NSFetchRequest(entityName: "ZoneMessage")
        
        let predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        if let result = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [ZoneMessage] {
            if (!result.isEmpty) {
                return result[0]
            }
        }
        
        return nil

    }
    
    func getFiltersForUser(userId: String) -> [Filter] {
        var error:NSError? = nil
        
        let fetchRequest = NSFetchRequest(entityName: "Filter")
        
        let predicate = NSPredicate(format: "user.id = %@", userId)
        fetchRequest.predicate = predicate

        let sortDescriptor = NSSortDescriptor(key: "category.name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let result = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [Filter] {
            return result
        }
        
        return [Filter]()

    }
    
    func getAllCategories() -> [Category] {
        var error:NSError? = nil
        
        let fetchRequest = NSFetchRequest(entityName: "Category")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]       
        
        if let result = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [Category] {
            return result
        }
        
        return [Category]()
    }
    
    func createNewFilter(category: Category, userId: String) -> Filter {
        
        let newFilter = NSEntityDescription.insertNewObjectForEntityForName("Filter", inManagedObjectContext: coreDataStack.managedObjectContext!) as Filter
        
        newFilter.category = category
        newFilter.active = true
        
        if let user = getUser(userId) {
            newFilter.user = user
        }
        
        
        return newFilter
        
    }
    
    func saveUser(user: NSDictionary) {
        
        let id = user["id"] as String
        
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        let predicate = NSPredicate(format: "id = %@", id)
        fetchRequest.predicate = predicate

        var error:NSError? = nil
        let result: [User]? = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [User]
        
        if (result != nil && !result!.isEmpty) {
            let existingUser = result![0]
            
            existingUser.populateAttributes(user)
            
            let zones = getZonesForUser(existingUser.id)
            existingUser.setZones(zones)

         } else {
            let newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: coreDataStack.managedObjectContext!) as User
            newUser.id = id
            newUser.populateAttributes(user)
            
            let zones = getZonesForUser(newUser.id)
            newUser.setZones(zones)
            
            error = nil
            coreDataStack.managedObjectContext!.save(&error)
            
            if (error != nil) {
                println("Error saving user with id \(id):  \(error)")
            }
        }
    }
    
    func getZonesForUser(userId: String) -> [Zone] {
        let fetchRequest = NSFetchRequest(entityName: "Zone")
        
        let predicate = NSPredicate(format: "zone_owner.id = %@", userId)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        if let fetchResult: [Zone] = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [Zone] {
            
            if (error == nil) {
                return fetchResult
                
                
            } else {
                println("Error fetching zones for user\(userId): \(error)")
            }
        }
        return [Zone]()
    }
    
    func getUser(userId: String) -> User? {
        let fetchRequest = NSFetchRequest(entityName: "User")
        
        let predicate = NSPredicate(format: "id = %@", userId)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        if let fetchResult: [User] = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [User] {
            
            if (error == nil && !fetchResult.isEmpty) {
                return fetchResult[0]
                
                
            } else {
                println("Error fetching zones for user\(userId): \(error)")
            }
        }
        return nil
    }
    
    func updateUser(userId: String, displayName: String?, email: String?, status: String?) {
        
        var error: NSError? = nil
        
        if let user = getUser(userId) {
            if (displayName != nil) {
                user.display_name = displayName!
            }
            if (email != nil) {
                user.email = email!
            }
            if (status != nil) {
                user.status = status!
            }
        }
        coreDataStack.managedObjectContext!.save(&error)
    }
    
    func getBeaconZone(major: NSNumber, minor: NSNumber) -> Zone? {
        let fetchRequest = NSFetchRequest(entityName: "Zone")
        
        let predicate = NSPredicate(format: "major = %@ AND minor = %@", major, minor)
        fetchRequest.predicate = predicate
        
        var error:NSError? = nil
        
        if let fetchResult: [Zone] = coreDataStack.managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [Zone] {
            
            if (error == nil && !fetchResult.isEmpty) {
                return fetchResult[0]
                
                
            } else {
                println("Error fetching zone for major \(major) minor \(minor): \(error)")
            }
        }
        return nil
    }
    
    func save() {
        coreDataStack.managedObjectContext!.save(nil)
    }
}
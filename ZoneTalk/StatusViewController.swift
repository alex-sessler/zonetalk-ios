//
//  StatusViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 13/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class StatusViewController: UIViewController {
    
    let webService: WebService = WebService.sharedInstance
    
    var currentStatus: String = "*** No Status ***"
    var activityIndicator: UIActivityIndicatorView?

    @IBOutlet weak var currentStatusLabel: UILabel!
    @IBOutlet weak var newStatusTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currentStatusLabel.text = currentStatus
        self.activityIndicator = ZoneTalkActivityIndicator(view: self.view)
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateStatusSucceeded:", name: Constants.NOTIFICATION_UPDATE_USER_SUCCEEDED, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    @IBAction func saveStatusTapped(sender: AnyObject) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            self.webService.saveUserInformation(nil, status: self.newStatusTextField.text, password: nil)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator!.startAnimating()
            })
        })
    }
    
    func updateStatusSucceeded(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        
        self.currentStatusLabel.text = self.newStatusTextField.text
        self.newStatusTextField.text = ""
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

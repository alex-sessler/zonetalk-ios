//
//  ZoneMapViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 15/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import UIKit

class ZoneMapViewController: UIViewController, GMSMapViewDelegate{
    
    var mapView: GMSMapView?
    let longPressRecognizer = UILongPressGestureRecognizer()
    let persistenceService = PersistenceService.sharedInstance
    var filters : [Filter]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tabBarHeight = self.tabBarController?.tabBar.bounds.height
        let navigationBarHeight = self.navigationController?.navigationBar.frame.size.height
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        
        let camera = GMSCameraPosition.cameraWithLatitude(50.541271, longitude: 10.300052, zoom: 6)
        
        mapView = GMSMapView.mapWithFrame(CGRectMake(0, (navigationBarHeight! + statusBarHeight), self.view.bounds.width, self.view.bounds.height - (navigationBarHeight! + tabBarHeight! + statusBarHeight)), camera: camera)
        
        let navBarHeight = self.navigationController!.navigationBar.frame.size.height

        if let map = mapView? {
            map.myLocationEnabled = true
            map.settings.compassButton = true
            map.settings.myLocationButton = true
            map.delegate = self
            
           map.addObserver(self, forKeyPath: "myLocation", options: .New, context: nil)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let mapV = self.mapView? {
                    mapV.myLocationEnabled = true
                }

            })
            
            longPressRecognizer.addTarget(self, action: "longPressedView")
            mapView?.addGestureRecognizer(longPressRecognizer)

            
            self.view.addSubview(mapView!)
            
            let zonesToDrawCircles = [Zone]()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.filters = PersistenceService.sharedInstance.getFiltersForUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "zonesFetched:", name: Constants.NOTIFICATION_ZONES_FETCHED, object: nil)
       
        self.persistenceService.getZones()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func mapView(mapView: GMSMapView!, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        println("tapped at coordinate: \(coordinate)")
    }
    
    
    // draw the zones on the map after the zones were all fetched from the core data store
    func zonesFetched(notification: NSNotification) {
        
        self.mapView!.clear()
        
        if let zones = notification.userInfo!["zones"] as? [Zone] {
            
            for zone in zones {
                
                let categoryToDisplayZoneUnder = getCategoryToDisplayZoneUnder(zone.main_category, otherCategories: zone.other_categories)
                if (categoryToDisplayZoneUnder != nil && zone.radius.integerValue > 0 && zone.main_category != NSNull()) {
                    drawCircle(zone.latitude.floatValue, longitude: zone.longitude.floatValue, radius: zone.radius.doubleValue, main_category: categoryToDisplayZoneUnder!)
                }
            }
        }
    }
    
    func getCategoryToDisplayZoneUnder(mainCategory: Category, otherCategories: NSSet) -> Category? {
        
        if (!isCategoryFiltered(mainCategory)) {
            return mainCategory
        }
        
        var categoriesOfZone = otherCategories.allObjects as Array<Category>
        
        var unfilteredCategories = [Category]()
        
        for (index, category) in enumerate(categoriesOfZone) {
            if (!isCategoryFiltered(category)) {
                println("category \(category.name) is not filtered")
                unfilteredCategories.append(category)
            }
        }
        
        if (unfilteredCategories.isEmpty) {
            return nil
        }
        
        unfilteredCategories.sort({$0.name < $1.name})
        
        return unfilteredCategories[0]
    }
    
    func isCategoryFiltered(category: Category) -> Bool{
        if let filters = self.filters {
            for filter in filters {
                if (filter.active.boolValue && filter.category.name == category.name) {
                    return true
                }
            }
        }
        return false
    }
    
    func drawCircle(latitude: Float, longitude: Float, radius: Double, main_category: Category) {
        
        var circleCenter: CLLocationCoordinate2D  = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),longitude: CLLocationDegrees(longitude))
        var circle :GMSCircle = GMSCircle(position: circleCenter, radius: radius)
        
        var colors = Constants.getColorForCategory(main_category.name)
        circle.strokeColor = colors.regularColor
        circle.fillColor = colors.lightColor        
        
        circle.strokeWidth = 1;
        
        circle.map = self.mapView!
    }
    
    func longPressedView(){
        let tapAlert = UIAlertController(title: "Long Pressed", message: "You just long pressed the long press view", preferredStyle: UIAlertControllerStyle.Alert)
        tapAlert.addAction(UIAlertAction(title: "OK", style: .Destructive, handler: nil))
        self.presentViewController(tapAlert, animated: true, completion: nil)
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        let location = change[NSKeyValueChangeNewKey] as CLLocation
        self.mapView!.camera = GMSCameraPosition.cameraWithTarget(location.coordinate, zoom: 14)
        self.mapView!.removeObserver(self, forKeyPath: "myLocation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
}

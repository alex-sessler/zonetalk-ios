//
//  WebService.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 19/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation

class WebService {
    
    class var sharedInstance : WebService {
        struct Static {
            static let instance : WebService = WebService()
        }
        return Static.instance
    }
    
    let persistenceService = PersistenceService.sharedInstance
    
    func updateZones(latitude: Double?, longitude: Double?, kmRadius: Int?) {
        
        if let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY) {
            
            var request: NSMutableURLRequest
            
            if (kmRadius == nil || latitude == nil || longitude == nil) {
                request = createRequest(Constants.URL_GET_ALL_ZONES, httpMethod: "GET", userCredentials: encryptedCredentials)
            } else {
                request = createRequest(Constants.getUrlGetZonesWithinRadius(latitude!, longitude: longitude!, radius: kmRadius!), httpMethod: "GET", userCredentials: encryptedCredentials)
            }
            
            let sharedSession = NSURLSession.sharedSession()
            
            let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
                if (error == nil) {
                    let dataObject = NSData(contentsOfURL: location)
                    
                    
                    if let data = dataObject? {
                        
                        var err: NSError?
                        
                        let zoneArray: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as Array<NSDictionary>
                        
                        if (err != nil) {
                            println("JSON Error \(err!.localizedDescription)")
                        }
                        else {
                            dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {
                                
                                self.persistenceService.persistZones(zoneArray)
                                
                                for zone in zoneArray {
                                    
                                    var zone_id: String?
                                    
                                    var tmp: NSNumber? = zone["id"] as? NSNumber
                                    
                                    if(tmp != nil) {
                                        zone_id = String(tmp!.integerValue)
                                    } else {
                                        zone_id = zone["id"] as? String
                                    }
                                    
                                    self.updateZoneMessage(zone_id!)
                                }
                            })
                        }
                        
                    } else {
                        println("error serializing data")
                    }
                    
                }
            })
            
            downloadTask.resume()
            
        }
    }
    
    func updateBeaconZones(){
        
        if let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY) {
            
            let request = createRequest(Constants.getUrlBeaconZones(), httpMethod: "GET", userCredentials: encryptedCredentials)
            
            let sharedSession = NSURLSession.sharedSession()
            
            let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
                if (error == nil) {
                    let dataObject = NSData(contentsOfURL: location)                    
                    
                    if let data = dataObject? {
                        
                        var err: NSError?
                        
                        let zoneArray: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as Array<NSDictionary>
                        
                        if (err != nil) {
                            println("JSON Error \(err!.localizedDescription)")
                        }
                        else {
                            dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {
                                
                                self.persistenceService.persistZones(zoneArray)
                                
                                for zone in zoneArray {
                                    
                                    var zone_id: String?
                                    
                                    var tmp: NSNumber? = zone["id"] as? NSNumber
                                    
                                    if(tmp != nil) {
                                        zone_id = String(tmp!.integerValue)
                                    } else {
                                        zone_id = zone["id"] as? String
                                    }
                                    
                                    self.updateZoneMessage(zone_id!)
                                }
                            })
                        }
                        
                    } else {
                        println("error serializing data")
                    }
                    
                }
            })
            
            downloadTask.resume()
            
        }

        
    }
    
    func updateZoneMessage(id: String) {
        var err: NSError?
        
        let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
        
        if (encryptedCredentials == nil) {
            // handle false credentials
            return
        }
        
        
        
        let request = createRequest(Constants.getUrlGetMessageForZone(id), httpMethod: "GET", userCredentials: encryptedCredentials!)
        
        let sharedSession = NSURLSession.sharedSession()
        
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            if (error == nil) {
                if let data = NSData(contentsOfURL: location) {
                    
                    let zoneMessageData: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSDictionary
                    
                    if (error == nil) {
                        self.persistenceService.saveZoneMessage(zoneMessageData, zoneId: id)
                    } else {
                        println("Update ZoneMessage failed: \(NSString(data: data, encoding: NSUTF8StringEncoding))")
                    }
                    
                    
                } else {
                    println("error serializing data")
                }
                
            }
            else {
                println("Update Zone Message Error: \(error)")
            }
        })
        
        downloadTask.resume()
        
        
    }
    
    func loginUser() {

        
        if let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY) {
            loginUser(encryptedCredentials, saveUser: false)
        } else {
            // handle error
        }
      }
    
    func loginUser(email: String, password: String) {
        
        let credentials = createBase64String(email, password: password)
        
        loginUser(credentials, saveUser: true)

    }
    
    func loginUser(credentials: String, saveUser: Bool) {
        
        
        var err: NSError?
        
        let request = createRequest(Constants.getUrlUser(), httpMethod: "GET", userCredentials: credentials)
        
        let sharedSession = NSURLSession.sharedSession()
        
        sharedSession.configuration.timeoutIntervalForRequest = 10
        
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            if (error == nil) {
                if let data = NSData(contentsOfURL: location) {
                    
                    if let userData: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary {
                        if (error == nil) {
                            
                            KeychainWrapper.setString(credentials, forKey: Constants.KEYCHAIN_USER_SECRET_KEY)
                            KeychainWrapper.setString(userData["id"] as String, forKey: Constants.KEYCHAIN_USER_ID_KEY)
                            
                            if (saveUser) {
                                self.persistenceService.saveUser(userData)
                            }

                            dispatch_async(dispatch_get_main_queue(), {
                                
                                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_SUCCEEDED, object: nil, userInfo: nil)
                            })

                        } else {
                            println("Login failed: \(NSString(data: data, encoding: NSUTF8StringEncoding))")
                            
                            KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
                            KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_ID_KEY)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message": "Wrong Email/Password"])
                            })
                        }
                        
                    } else {
                        println("Login failed: \(NSString(data: data, encoding: NSUTF8StringEncoding))")
                        
                        KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
                        KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_ID_KEY)
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message": "Wrong Email/Password"])
                        })
                    }
                    
                    
                } else {
                    println("error serializing data")
                }
                
            } else {
                
                
                
                println("Login Error \(error)")
     
                if (error.code == -1013) {
                    KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
                    KeychainWrapper.removeObjectForKey(Constants.KEYCHAIN_USER_ID_KEY)
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message":"Wrong Email/Password"])
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message":"Network Error"])
                    })
                }
            }
        })
        
        downloadTask.resume()

    }
    
    func signUpUser(displayName: String, email: String, password: String) {
        
        var err: NSError?
        
        var userData = ["displayName":displayName, "password":password, "email":email] as [String: String]
        let data = ("displayName=\(displayName)&password=\(password)&email=\(email)" as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let url = NSURL(string: Constants.getUrlSignUp())
        
        let request = NSMutableURLRequest(URL: url!)

        request.HTTPMethod = "POST"
        
        request.HTTPBody = data
        let sharedSession = NSURLSession.sharedSession()
        
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            if (error == nil) {
                if let data = NSData(contentsOfURL: location) {
                    
                    if let userData: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary {
                        if (error == nil) {
                            
                            KeychainWrapper.setString(self.createBase64String(email, password: password), forKey: Constants.KEYCHAIN_USER_SECRET_KEY)
                            KeychainWrapper.setString(userData["id"] as String, forKey: Constants.KEYCHAIN_USER_ID_KEY)
                            
                            self.persistenceService.saveUser(userData)
                            
                            dispatch_async(dispatch_get_main_queue(), {

                                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_SIGN_UP_SUCCEEDED, object: nil, userInfo: nil)

                            })
                            
                        } else {
                            println("Sign Up Error: \(error)")
                            dispatch_async(dispatch_get_main_queue(), {
                                NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message":"Email Address already exists"])
                            })
                        }
                        
                    } else {
                        println("SignUp failed: \(NSString(data: data, encoding: NSUTF8StringEncoding)) \n Error:\(error)")
                       
                        dispatch_async(dispatch_get_main_queue(), {
                            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_SIGN_UP_FAILED, object: nil, userInfo: ["message":"Email Address already exists"])
                        })
                    }
                    
                    
                } else {
                    println("error serializing data")
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_SIGN_UP_FAILED, object: nil, userInfo: ["message":"Network Error"])
                    })
                }
                
            } else {
                println("Error: \(error)")
                if (error.code == -1004) {
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOGIN_FAILED, object: nil, userInfo: ["message":"Email Address already exists"])
                    })
                } else {
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_SIGN_UP_FAILED, object: nil, userInfo: ["message":"Network Error"])
                    })
                }
            }
        })
        
        downloadTask.resume()
   
    }
    
    func saveUserInformation(displayName: String?,  status: String?, password: String?) {
        
        var err : NSError? = nil
        
        var user = self.persistenceService.getUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)!
        
        var arguments = ""
        if (displayName != nil) {
            arguments += "displayName=\(displayName!)&"
        }
        if (status != nil) {
            arguments += "status=\(status!)&"
        }
        if (password != nil) {
            arguments += "password=\(createBase64String(user.email!, password: password!))&"
        }
        
        let data = (arguments.substringToIndex(arguments.endIndex.predecessor()) as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
        if (encryptedCredentials == nil) {
           // logout
            return
        }
        
        let request = createRequest(Constants.getUrlUser(), httpMethod: "POST", userCredentials: encryptedCredentials!)
        request.HTTPBody = data
        
        let sharedSession = NSURLSession.sharedSession()
        
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            if (error == nil) {
                if ((response as NSHTTPURLResponse).statusCode == 200) {
                    
                    if (password == nil) {
                        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {
                            self.persistenceService.updateUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!, displayName: displayName, email: nil, status: status)
                        })
                    } else {
                        // password changed -> update Keychain credentials
                        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), {
                            
                            let success = KeychainWrapper.setString(self.createBase64String(user.email!, password: password!), forKey: Constants.KEYCHAIN_USER_SECRET_KEY)
                           
                            
                        })
                    }
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_SUCCEEDED, object: nil, userInfo: nil)
                        
                    })
                }
                dispatch_async(dispatch_get_main_queue(), {
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_FAILED, object: nil, userInfo: nil)
                    
                })
            }
            else {
                println("Save User Information Error: \(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_FAILED, object: nil, userInfo: nil)
                    
                })
            }
        })
        
        downloadTask.resume() 
    }
    
    func changeUserEmail(email: String, password: String?) {
        
        var user = self.persistenceService.getUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)!
        
         let arguments = "email=\(email)&"
        
        
        let data = (arguments.substringToIndex(arguments.endIndex.predecessor()) as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let encryptedCredentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
        if (encryptedCredentials == nil) {
            // logout
            return
        }
        
        let request = createRequest(Constants.getUrlUser(), httpMethod: "POST", userCredentials: encryptedCredentials!)
        request.HTTPBody = data
        
        let sharedSession = NSURLSession.sharedSession()
        
        let downloadTask: NSURLSessionDownloadTask = sharedSession.downloadTaskWithRequest(request, completionHandler: { (location: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
            if (error == nil) {
                if ((response as NSHTTPURLResponse).statusCode == 200) {
                    
                    if (password != nil) {
                         KeychainWrapper.setString(self.createBase64String(email, password: password!), forKey: Constants.KEYCHAIN_USER_SECRET_KEY)
                    }
                    
                    self.persistenceService.updateUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!, displayName: nil, email: email, status: nil)

                    dispatch_async(dispatch_get_main_queue(), {
                        
                        NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_SUCCEEDED, object: nil, userInfo: nil)
                    })
                }
                dispatch_async(dispatch_get_main_queue(), {
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_FAILED, object: nil, userInfo: nil)
                    
                })
            }
            else {
                println("Change Email Error: \(error)")
                dispatch_async(dispatch_get_main_queue(), {
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_UPDATE_USER_FAILED, object: nil, userInfo: nil)
                    
                })
            }
        })
        
        downloadTask.resume()
        
    }
    
    func createRequest(urlString: String, httpMethod: String, userCredentials: String) -> NSMutableURLRequest {
        
        let url = NSURL(string: urlString)
        
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = httpMethod
        request.setValue("Basic \(userCredentials)", forHTTPHeaderField: "Authorization")
        
        
        return request
    }
    
    func createBase64String(email: String, password: String) -> String {
        let loginString = NSString(format: "%@:%@", email, password)
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions(nil)
        return base64LoginString
    }
}

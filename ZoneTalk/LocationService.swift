//
//  LocationService.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 30/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation

class LocationService: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    class var sharedInstance : LocationService {
        struct Static {
            static let instance : LocationService = LocationService()
        }
        return Static.instance
    }
    
    override init() {
        super.init()
        initializeLocationManager()
    }
    
    func initializeLocationManager() {
        if (CLLocationManager.locationServicesEnabled()) {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        } else {
            self.locationManager.requestAlwaysAuthorization()
            initializeLocationManager()
        }
    }
    
    func startUpdatingLocation() {
        if (CLLocationManager.locationServicesEnabled()) {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if let currentLocation = locations.last as? CLLocation {
            self.currentLocation = currentLocation
            NSNotificationCenter.defaultCenter().postNotificationName(Constants.NOTIFICATION_LOCATION_UPDATED, object: nil)
        }
    }
    
    
    
    
}

//
//  MapPreviewViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 23/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class MapPreviewViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate {
    

    
    var zones : [ZoneTableItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var nib = UINib(nibName: "MapPreviewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "MapPreviewCell")
    }
    
    override func viewWillAppear(animated: Bool) {
            
        self.tableView.backgroundColor = UIColor.whiteColor()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countElements(self.zones)
 
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("MapPreviewCell") as MapPreviewCell
        
        let zone = self.zones[indexPath.row]
        
        cell.loadData(zone.mainCategory, title: zone.title, zoneId: zone.id)
       
        return cell
    }
}

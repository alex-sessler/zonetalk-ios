//
//  ZoneTableHeaderCell.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 03/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation
import UIKit

class ZoneTableHeaderCell: UITableViewCell  {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var numberOfZones: UILabel!
    
    func loadItem(categoryName: String, categoryImageUrl: String, numberOfZones: String) {
        self.categoryName.text = categoryName
        self.categoryImage.image = UIImage(named: categoryImageUrl)
        self.numberOfZones.text = numberOfZones
        
        // May change        
        self.categoryName.textColor = UIColor.whiteColor()
        
        self.numberOfZones.textColor = Constants.ZONETALK_COLOR
        self.numberOfZones.backgroundColor = UIColor.whiteColor()
        self.numberOfZones.layer.cornerRadius = 15
        self.numberOfZones.layer.masksToBounds = true  
    }
}



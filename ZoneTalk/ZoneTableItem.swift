//
//  zoneTableItem.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 03/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation

struct ZoneTableItem {
    let id: String
    let title : String
    let distance : String?
    let mainCategory : String
    let ownerId: String
}
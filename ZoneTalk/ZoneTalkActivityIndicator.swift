//
//  ZoneTalkActivityIndicator.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 13/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

class ZoneTalkActivityIndicator: UIActivityIndicatorView {
    
    var view: UIView = UIView()
    
    init (view: UIView) {
        self.view = view
        super.init(frame: view.frame)
        
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.frame = self.view.frame
        self.center = self.view.center
        self.hidesWhenStopped = true
        self.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func startAnimating() {
        view.addSubview(self)
        super.startAnimating()
    }
    
    override func stopAnimating() {
        super.stopAnimating()
        super.removeFromSuperview()
    }


    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

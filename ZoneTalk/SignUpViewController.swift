//
//  SignUpViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 10/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    var activityIndicator: UIActivityIndicatorView?
    let webService = WebService.sharedInstance

    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.displayNameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
        
        self.activityIndicator = ZoneTalkActivityIndicator(view: self.view)
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "signupSucceeded:", name: Constants.NOTIFICATION_SIGN_UP_SUCCEEDED, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "signupFailed:", name: Constants.NOTIFICATION_SIGN_UP_FAILED, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func signUpTapped(sender: UIButton) {
        
        if (passwordTextField.text != confirmPasswordTextField.text) {
            showPasswordsDontMatch()
            return
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            self.webService.signUpUser(self.displayNameTextField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), email: self.emailTextField.text, password: self.passwordTextField.text)
            
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator!.startAnimating()
            })
            
        })
      
        
    }
    
    func showPasswordsDontMatch() {
        
        self.infoIcon.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
        self.infoLabel.textColor = UIColor.redColor()
        self.infoLabel.text = "Passwords don't match"
        
        self.passwordTextField.text = ""
        self.confirmPasswordTextField.text = ""
        
        self.passwordTextField.layer.borderWidth = 1
        self.passwordTextField.layer.borderColor = UIColor.redColor().CGColor
        self.passwordTextField.attributedPlaceholder! = NSAttributedString(string:"Password",
            attributes:[NSForegroundColorAttributeName: UIColor.redColor()])
        
        self.confirmPasswordTextField.layer.borderWidth = 1
        self.confirmPasswordTextField.layer.borderColor = UIColor.redColor().CGColor
        self.confirmPasswordTextField.attributedPlaceholder! = NSAttributedString(string:"Password",
            attributes:[NSForegroundColorAttributeName: UIColor.redColor()])


    }
    
    func signupSucceeded(notification: NSNotification) {
       
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        
        let delegate = UIApplication.sharedApplication().delegate!
        if let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UIViewController {
            delegate.window!!.rootViewController = initialViewController
        }
    }
    
    func signupFailed(notification: NSNotification) {
       
        dispatch_async(dispatch_get_main_queue(), {
            self.activityIndicator!.stopAnimating()
        })
        
        self.infoIcon.image = UIImage(named: Constants.IMAGE_ICON_ERROR)
        self.infoLabel.textColor = UIColor.redColor()
        self.infoLabel.text = (notification.userInfo!["message"] as String)
        
        self.emailTextField.text = ""
        self.displayNameTextField.text = ""
        self.passwordTextField.text = ""
        self.confirmPasswordTextField.text = ""
        
    }

    /* KEYBOARD UTIL */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

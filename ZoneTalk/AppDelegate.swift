//
//  AppDelegate.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 10/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let zoneUpdateService = ZoneUpdateService()
    let locationManager = CLLocationManager()
    let locationService = LocationService.sharedInstance
    let webService = WebService.sharedInstance
    let beaconManager = iBeaconService()
    

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        // configure Google Maps API Key
        GMSServices.provideAPIKey(Constants.GMAPS_API_KEY);
        
        let authstate = CLLocationManager.authorizationStatus()
        if(authstate == CLAuthorizationStatus.NotDetermined || authstate == CLAuthorizationStatus.Denied){
            locationManager.requestAlwaysAuthorization()
        }
        
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert, categories: nil))
        
        let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        

        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: Constants.ZONETALK_COLOR], forState:.Selected)
        
        FBLoginView.self
        FBProfilePictureView.self
        
        locationService.startUpdatingLocation()
        
        
        let credentials = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_SECRET_KEY)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if (credentials != nil) {
            if let regularRootController = storyboard.instantiateInitialViewController() as? UIViewController {
                self.window!.rootViewController = regularRootController
            }
        } else {
            if let loginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewNavigationController") as? UINavigationController {
                self.window!.rootViewController = loginViewController
            }
        }
        
        
        return true
    }
    
    func application(application: UIApplication,
        performFetchWithCompletionHandler completionHandler:
        ((UIBackgroundFetchResult) -> Void)!){
            
            
           /*
            // background fetch code here
            let date = NSDate()
            let formatter = NSDateFormatter()
            formatter.timeStyle = .ShortStyle
            println("successful background fetch: " + formatter.stringFromDate(date))
            
            
            var localNotification:UILocalNotification = UILocalNotification()
            localNotification.alertAction = "Testing notifications on iOS8"
            localNotification.alertBody = "Woww it works!!"
            localNotification.fireDate = NSDate(timeIntervalSinceNow: 30)
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            completionHandler(.NewData)*/
            
            
            
           
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: NSString?, annotation: AnyObject) -> Bool {
        var wasHandled:Bool = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
        return wasHandled
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        ZoneTalkCoreDataStack.sharedInstance.saveContext()
        self.beaconManager.stopMonitoringBeacons()
        self.zoneUpdateService.stopUpdatingZones()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let authstate = CLLocationManager.authorizationStatus()
        if(authstate == CLAuthorizationStatus.NotDetermined || authstate == CLAuthorizationStatus.Denied ){
            locationManager.requestAlwaysAuthorization()
        }
        self.beaconManager.startMonitoringBeacons()
        self.zoneUpdateService.startUpdatingZones()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        ZoneTalkCoreDataStack.sharedInstance.saveContext()
    }
}


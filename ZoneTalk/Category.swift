//
//  Category.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 29/12/14.
//  Copyright (c) 2014 zonetalk. All rights reserved.
//

import Foundation
import CoreData

class Category: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var zones_other: NSSet
    @NSManaged var zones_main: NSSet

}

//
//  FilterCollectionViewCell.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 05/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import Foundation
import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterImage: UIImageView!
    
    @IBOutlet weak var filterName: UILabel!
    
    func loadData(categoryName: String, active: Bool) {
        
        filterName.text = categoryName
        
        filterImage.image = UIImage(named: Constants.getCategoryImageUrlForName(categoryName, inverse: !active))
    }
    
}

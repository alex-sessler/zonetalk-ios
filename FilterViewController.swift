//
//  FilterViewController.swift
//  ZoneTalk
//
//  Created by Alexander Sessler on 05/01/15.
//  Copyright (c) 2015 zonetalk. All rights reserved.
//

import UIKit

let reuseIdentifier = "ˇ"

class FilterViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    let persistenceService = PersistenceService.sharedInstance
    
    var categories : [Category] = []
    var filters: [Filter] = []
    
    @IBOutlet var filterCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userId = KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY) {
            if (userId == Constants.GUEST_USER_ID) {
                
                let overlay: UIView = UIView(frame: self.view.frame)
                overlay.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                
                var label = UILabel(frame: CGRectMake(0, 0, self.view.bounds.width, 21))
                label.center = CGPointMake((self.view.bounds.width/2), (self.view.bounds.height/2))
                label.textAlignment = NSTextAlignment.Center
                label.textColor = UIColor.whiteColor()
                label.text = "Only for registered Users"
                
                overlay.addSubview(label)
                
                self.view.addSubview(overlay)
                
            }
        }
        
        let categories = persistenceService.getAllCategories()

        self.categories =  categories.sorted({$0.name < $1.name})
        self.filters = persistenceService.getFiltersForUser(KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)

        // Register cell classes
        self.filterCollectionView.registerNib(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")

    }
    
    override func viewWillDisappear(animated: Bool) {
        persistenceService.save()
    }




    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    /* COLLECTION VIEW */
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }

    // configure the cell
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FilterCollectionViewCell", forIndexPath: indexPath) as FilterCollectionViewCell
        
        let categoryName = categories[indexPath.row].name
        
        var active = false
        
        for filter in filters {
            if (filter.active.boolValue && filter.category.name == categoryName) {
                active = true
                break
            }
        }
        
        cell.loadData(categoryName, active: active)
    
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as FilterCollectionViewCell
        
        let categoryName = cell.filterName.text
        
        var filterExists = false
        for filter in filters {
            if (filter.category.name == categoryName) {
                filterExists = true
                filter.active = !filter.active.boolValue
                cell.filterImage.image = UIImage(named: Constants.getCategoryImageUrlForName(filter.category.name, inverse: !filter.active.boolValue))
                break
            }
        }
        if (!filterExists) {
            for category in categories {
                if category.name == categoryName {
                    var newFilter = persistenceService.createNewFilter(category, userId: KeychainWrapper.stringForKey(Constants.KEYCHAIN_USER_ID_KEY)!)
                    filters.append(newFilter)
                    cell.filterImage.image = UIImage(named: Constants.getCategoryImageUrlForName(newFilter.category.name, inverse: !newFilter.active.boolValue))
                    break
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
